FROM python:3.6
MAINTAINER douglasirungu57@gmail.com

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APPWORKINGDIR=/wazee
ENV SERVERPORT=8000
ENV TOTALWORKERS=4


COPY . $APPWORKINGDIR

WORKDIR $APPWORKINGDIR

COPY requirements.txt ./
RUN apt-get update
RUN apt update -y && apt install -y \
libsm6 \
libxext6 \
libxrender-dev
RUN pip3 install -r requirements.txt
#RUN pip install --force-reinstall opencv-contrib-python==4.1.2.30
RUN pip install --no-cache --force-reinstall opencv-contrib-python==4.0.0.21

#ENTRYPOINT gunicorn edms.wsgi:application - bind  0.0.0.0:$SERVERPORT
#ENTRYPOINT python3 manage.py runserver 0.0.0.0:$SERVERPORT


#EXPOSE $SERVERPORT
