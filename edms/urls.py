"""edms URL Configuration

The `urlpatterns` list routes URLs to view. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function view
    1. Add an import:  from my_app import view
    2. Add a URL to urlpatterns:  path('', view.home, name='home')
Class-based view
    1. Add an import:  from other_app.view import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from edms import views as app_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app.urls')),
    #url(r'facerecog', app_views.index,name='facerecog_index'),
    url(r'^error_image$', app_views.errorImg),
    url(r'^create_dataset$', app_views.create_dataset),
    path('create_dataset_edms/<int:userId>', app_views.create_dataset_edms,name='create_dataset_edms'),
    path('detect_edms/', app_views.detect_edms,name='detect_edms'),
    path('verify_withdrawal/<int:user_pk>/', app_views.verify_withdrawal,name='verify_withdrawal'),
    path('trainer', app_views.trainer,name='face_train'),
    url(r'^eigen_train$', app_views.eigenTrain),
    url(r'^detect$', app_views.detect),
    url(r'^detect_image$', app_views.detectImage),
    url(r'session_security/', include('session_security.urls')),
    url(r'^records/', include('records.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


