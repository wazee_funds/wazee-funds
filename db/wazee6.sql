PGDMP     7                    y            wazee6    12.2    12.2 G   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    56971    wazee6    DATABASE     �   CREATE DATABASE wazee6 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE wazee6;
                postgres    false            �            1259    57132 	   app_batch    TABLE     {  CREATE TABLE public.app_batch (
    id integer NOT NULL,
    batch_no character varying(255) NOT NULL,
    batch_type character varying(255) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    is_return_batch boolean NOT NULL,
    state character varying(50) NOT NULL,
    description text,
    miscellaneous jsonb,
    created_by_id integer,
    group_id integer
);
    DROP TABLE public.app_batch;
       public         heap    postgres    false            �            1259    57130    app_batch_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_batch_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.app_batch_id_seq;
       public          postgres    false    221            �           0    0    app_batch_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.app_batch_id_seq OWNED BY public.app_batch.id;
          public          postgres    false    220            �            1259    57145    app_documentfile    TABLE     [  CREATE TABLE public.app_documentfile (
    id integer NOT NULL,
    file_reference character varying(100) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    state character varying(50) NOT NULL,
    file_barcode character varying(255) NOT NULL,
    flagged boolean NOT NULL,
    lock boolean NOT NULL,
    file_path character varying(100),
    stage character varying(50) NOT NULL,
    miscellaneous jsonb,
    assigned_to_id integer,
    batch_id integer NOT NULL,
    file_created_by_id integer NOT NULL,
    file_type_id character varying(100) NOT NULL,
    original_batch_id integer
);
 $   DROP TABLE public.app_documentfile;
       public         heap    postgres    false            �            1259    57143    app_documentfile_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_documentfile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.app_documentfile_id_seq;
       public          postgres    false    223            �           0    0    app_documentfile_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.app_documentfile_id_seq OWNED BY public.app_documentfile.id;
          public          postgres    false    222            �            1259    57237    app_documentfiledetail    TABLE     Z  CREATE TABLE public.app_documentfiledetail (
    id integer NOT NULL,
    document_barcode character varying(255) NOT NULL,
    document_name character varying(255) NOT NULL,
    document_content jsonb,
    document_file_path character varying(100),
    created_on timestamp with time zone NOT NULL,
    flagged boolean NOT NULL,
    state character varying(50) NOT NULL,
    passed_qa boolean NOT NULL,
    passed_validated boolean NOT NULL,
    miscellaneous jsonb,
    assigned_to_id integer,
    doc_created_by_id integer,
    document_type_id character varying(255),
    file_reference integer
);
 *   DROP TABLE public.app_documentfiledetail;
       public         heap    postgres    false            �            1259    57235    app_documentfiledetail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_documentfiledetail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.app_documentfiledetail_id_seq;
       public          postgres    false    241            �           0    0    app_documentfiledetail_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.app_documentfiledetail_id_seq OWNED BY public.app_documentfiledetail.id;
          public          postgres    false    240            �            1259    57158    app_documentfiletype    TABLE     �   CREATE TABLE public.app_documentfiletype (
    file_type character varying(100) NOT NULL,
    file_description character varying(255) NOT NULL
);
 (   DROP TABLE public.app_documentfiletype;
       public         heap    postgres    false            �            1259    57163    app_documenttype    TABLE     �   CREATE TABLE public.app_documenttype (
    document_name character varying(255) NOT NULL,
    document_field_specs jsonb NOT NULL,
    document_description character varying(255) NOT NULL
);
 $   DROP TABLE public.app_documenttype;
       public         heap    postgres    false            �            1259    57229 	   app_filer    TABLE     �   CREATE TABLE public.app_filer (
    id integer NOT NULL,
    filepond character varying(100) NOT NULL,
    document_reference character varying(40),
    file_reference_id integer NOT NULL
);
    DROP TABLE public.app_filer;
       public         heap    postgres    false            �            1259    57227    app_filer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_filer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.app_filer_id_seq;
       public          postgres    false    239            �           0    0    app_filer_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.app_filer_id_seq OWNED BY public.app_filer.id;
          public          postgres    false    238            �            1259    57221    app_modification    TABLE       CREATE TABLE public.app_modification (
    id integer NOT NULL,
    modified_from_stage character varying(50) NOT NULL,
    modified_to_stage character varying(50),
    created_at timestamp with time zone NOT NULL,
    by_id integer NOT NULL,
    file_id integer NOT NULL
);
 $   DROP TABLE public.app_modification;
       public         heap    postgres    false            �            1259    57219    app_modification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_modification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.app_modification_id_seq;
       public          postgres    false    237            �           0    0    app_modification_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.app_modification_id_seq OWNED BY public.app_modification.id;
          public          postgres    false    236            �            1259    57213    app_money_disburdment    TABLE     �   CREATE TABLE public.app_money_disburdment (
    id integer NOT NULL,
    created_at timestamp with time zone,
    sent_at timestamp with time zone,
    user_id integer NOT NULL
);
 )   DROP TABLE public.app_money_disburdment;
       public         heap    postgres    false            �            1259    57211    app_money_disburdment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_money_disburdment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.app_money_disburdment_id_seq;
       public          postgres    false    235            �           0    0    app_money_disburdment_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.app_money_disburdment_id_seq OWNED BY public.app_money_disburdment.id;
          public          postgres    false    234            �            1259    57173    app_notification    TABLE     �   CREATE TABLE public.app_notification (
    id integer NOT NULL,
    comment text,
    created_at timestamp with time zone,
    resolved boolean NOT NULL,
    created_by_id integer,
    file_id integer,
    resolved_by_id integer
);
 $   DROP TABLE public.app_notification;
       public         heap    postgres    false            �            1259    57171    app_notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.app_notification_id_seq;
       public          postgres    false    227            �           0    0    app_notification_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.app_notification_id_seq OWNED BY public.app_notification.id;
          public          postgres    false    226            �            1259    57205    app_notificationsentto    TABLE     �   CREATE TABLE public.app_notificationsentto (
    id integer NOT NULL,
    read_at timestamp with time zone,
    notification_id integer NOT NULL,
    user_id integer NOT NULL
);
 *   DROP TABLE public.app_notificationsentto;
       public         heap    postgres    false            �            1259    57203    app_notificationsentto_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_notificationsentto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.app_notificationsentto_id_seq;
       public          postgres    false    233            �           0    0    app_notificationsentto_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.app_notificationsentto_id_seq OWNED BY public.app_notificationsentto.id;
          public          postgres    false    232            �            1259    57195    app_profile    TABLE       CREATE TABLE public.app_profile (
    id integer NOT NULL,
    id_no character varying(25),
    phone character varying(25),
    full_name character varying(25),
    first_login boolean NOT NULL,
    image character varying(100),
    elderly_id integer,
    user_id integer NOT NULL
);
    DROP TABLE public.app_profile;
       public         heap    postgres    false            �            1259    57193    app_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.app_profile_id_seq;
       public          postgres    false    231            �           0    0    app_profile_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.app_profile_id_seq OWNED BY public.app_profile.id;
          public          postgres    false    230            �            1259    57184 	   app_stock    TABLE     D  CREATE TABLE public.app_stock (
    id integer NOT NULL,
    file_number character varying(50),
    comment text,
    name character varying(80) NOT NULL,
    nationality character varying(50) NOT NULL,
    cross_reference character varying(60) NOT NULL,
    file_category character varying(60) NOT NULL,
    date_last_correspondence timestamp with time zone,
    date_first_correspondence timestamp with time zone,
    location_of_file character varying(40) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    created_by_id integer
);
    DROP TABLE public.app_stock;
       public         heap    postgres    false            �            1259    57182    app_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_stock_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.app_stock_id_seq;
       public          postgres    false    229            �           0    0    app_stock_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.app_stock_id_seq OWNED BY public.app_stock.id;
          public          postgres    false    228            �            1259    57003 
   auth_group    TABLE     f   CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE public.auth_group;
       public         heap    postgres    false            �            1259    57001    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public          postgres    false    209            �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;
          public          postgres    false    208            �            1259    57013    auth_group_permissions    TABLE     �   CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         heap    postgres    false            �            1259    57011    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public          postgres    false    211            �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;
          public          postgres    false    210            �            1259    56995    auth_permission    TABLE     �   CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         heap    postgres    false            �            1259    56993    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public          postgres    false    207            �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;
          public          postgres    false    206            �            1259    57021 	   auth_user    TABLE     �  CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         heap    postgres    false            �            1259    57031    auth_user_groups    TABLE        CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         heap    postgres    false            �            1259    57029    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public          postgres    false    215            �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;
          public          postgres    false    214            �            1259    57019    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public          postgres    false    213            �           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;
          public          postgres    false    212            �            1259    57039    auth_user_user_permissions    TABLE     �   CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         heap    postgres    false            �            1259    57037 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public          postgres    false    217            �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;
          public          postgres    false    216            �            1259    57402    authtoken_token    TABLE     �   CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);
 #   DROP TABLE public.authtoken_token;
       public         heap    postgres    false            �            1259    57434    background_task    TABLE     �  CREATE TABLE public.background_task (
    id integer NOT NULL,
    task_name character varying(190) NOT NULL,
    task_params text NOT NULL,
    task_hash character varying(40) NOT NULL,
    verbose_name character varying(255),
    priority integer NOT NULL,
    run_at timestamp with time zone NOT NULL,
    repeat bigint NOT NULL,
    repeat_until timestamp with time zone,
    queue character varying(190),
    attempts integer NOT NULL,
    failed_at timestamp with time zone,
    last_error text NOT NULL,
    locked_by character varying(64),
    locked_at timestamp with time zone,
    creator_object_id integer,
    creator_content_type_id integer,
    CONSTRAINT background_task_creator_object_id_check CHECK ((creator_object_id >= 0))
);
 #   DROP TABLE public.background_task;
       public         heap    postgres    false            �            1259    57422    background_task_completedtask    TABLE       CREATE TABLE public.background_task_completedtask (
    id integer NOT NULL,
    task_name character varying(190) NOT NULL,
    task_params text NOT NULL,
    task_hash character varying(40) NOT NULL,
    verbose_name character varying(255),
    priority integer NOT NULL,
    run_at timestamp with time zone NOT NULL,
    repeat bigint NOT NULL,
    repeat_until timestamp with time zone,
    queue character varying(190),
    attempts integer NOT NULL,
    failed_at timestamp with time zone,
    last_error text NOT NULL,
    locked_by character varying(64),
    locked_at timestamp with time zone,
    creator_object_id integer,
    creator_content_type_id integer,
    CONSTRAINT background_task_completedtask_creator_object_id_check CHECK ((creator_object_id >= 0))
);
 1   DROP TABLE public.background_task_completedtask;
       public         heap    postgres    false            �            1259    57420 $   background_task_completedtask_id_seq    SEQUENCE     �   CREATE SEQUENCE public.background_task_completedtask_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.background_task_completedtask_id_seq;
       public          postgres    false    244            �           0    0 $   background_task_completedtask_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.background_task_completedtask_id_seq OWNED BY public.background_task_completedtask.id;
          public          postgres    false    243            �            1259    57432    background_task_id_seq    SEQUENCE     �   CREATE SEQUENCE public.background_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.background_task_id_seq;
       public          postgres    false    246            �           0    0    background_task_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.background_task_id_seq OWNED BY public.background_task.id;
          public          postgres    false    245            �            1259    57484    bank_account    TABLE     �   CREATE TABLE public.bank_account (
    id integer NOT NULL,
    balance numeric(12,2) NOT NULL,
    user_id integer NOT NULL
);
     DROP TABLE public.bank_account;
       public         heap    postgres    false            �            1259    57482    bank_account_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bank_account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.bank_account_id_seq;
       public          postgres    false    248            �           0    0    bank_account_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.bank_account_id_seq OWNED BY public.bank_account.id;
          public          postgres    false    247            �            1259    57502    bank_deposit    TABLE     %  CREATE TABLE public.bank_deposit (
    id integer NOT NULL,
    month character varying(255) NOT NULL,
    year character varying(255) NOT NULL,
    amount numeric(12,2) NOT NULL,
    balance numeric(12,2) NOT NULL,
    at timestamp with time zone NOT NULL,
    account_id integer NOT NULL
);
     DROP TABLE public.bank_deposit;
       public         heap    postgres    false            �            1259    57500    bank_deposit_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bank_deposit_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.bank_deposit_id_seq;
       public          postgres    false    252            �           0    0    bank_deposit_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.bank_deposit_id_seq OWNED BY public.bank_deposit.id;
          public          postgres    false    251            �            1259    57494    bank_withdraw    TABLE       CREATE TABLE public.bank_withdraw (
    id integer NOT NULL,
    amount numeric(12,2) NOT NULL,
    balance numeric(12,2) NOT NULL,
    at timestamp with time zone NOT NULL,
    account_id integer NOT NULL,
    by_agent_id integer NOT NULL,
    by_user_id integer NOT NULL
);
 !   DROP TABLE public.bank_withdraw;
       public         heap    postgres    false            �            1259    57492    bank_withdraw_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bank_withdraw_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.bank_withdraw_id_seq;
       public          postgres    false    250            �           0    0    bank_withdraw_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.bank_withdraw_id_seq OWNED BY public.bank_withdraw.id;
          public          postgres    false    249            �            1259    57099    django_admin_log    TABLE     �  CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         heap    postgres    false            �            1259    57097    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public          postgres    false    219            �           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;
          public          postgres    false    218            �            1259    56985    django_content_type    TABLE     �   CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         heap    postgres    false            �            1259    56983    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public          postgres    false    205            �           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;
          public          postgres    false    204            �            1259    56974    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         heap    postgres    false            �            1259    56972    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public          postgres    false    203            �           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
          public          postgres    false    202            �            1259    57549    django_session    TABLE     �   CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         heap    postgres    false            �            1259    57540    records_records    TABLE     �  CREATE TABLE public.records_records (
    id character varying(100) NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50),
    residence character varying(50),
    country character varying(50),
    education character varying(150),
    occupation character varying(150),
    marital_status character varying(50),
    bio text NOT NULL,
    recorded_at timestamp with time zone NOT NULL
);
 #   DROP TABLE public.records_records;
       public         heap    postgres    false            5           2604    57135    app_batch id    DEFAULT     l   ALTER TABLE ONLY public.app_batch ALTER COLUMN id SET DEFAULT nextval('public.app_batch_id_seq'::regclass);
 ;   ALTER TABLE public.app_batch ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220    221            6           2604    57148    app_documentfile id    DEFAULT     z   ALTER TABLE ONLY public.app_documentfile ALTER COLUMN id SET DEFAULT nextval('public.app_documentfile_id_seq'::regclass);
 B   ALTER TABLE public.app_documentfile ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    222    223            >           2604    57240    app_documentfiledetail id    DEFAULT     �   ALTER TABLE ONLY public.app_documentfiledetail ALTER COLUMN id SET DEFAULT nextval('public.app_documentfiledetail_id_seq'::regclass);
 H   ALTER TABLE public.app_documentfiledetail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    240    241    241            =           2604    57232    app_filer id    DEFAULT     l   ALTER TABLE ONLY public.app_filer ALTER COLUMN id SET DEFAULT nextval('public.app_filer_id_seq'::regclass);
 ;   ALTER TABLE public.app_filer ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    238    239            <           2604    57224    app_modification id    DEFAULT     z   ALTER TABLE ONLY public.app_modification ALTER COLUMN id SET DEFAULT nextval('public.app_modification_id_seq'::regclass);
 B   ALTER TABLE public.app_modification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    237    236    237            ;           2604    57216    app_money_disburdment id    DEFAULT     �   ALTER TABLE ONLY public.app_money_disburdment ALTER COLUMN id SET DEFAULT nextval('public.app_money_disburdment_id_seq'::regclass);
 G   ALTER TABLE public.app_money_disburdment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    235    235            7           2604    57176    app_notification id    DEFAULT     z   ALTER TABLE ONLY public.app_notification ALTER COLUMN id SET DEFAULT nextval('public.app_notification_id_seq'::regclass);
 B   ALTER TABLE public.app_notification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226    227            :           2604    57208    app_notificationsentto id    DEFAULT     �   ALTER TABLE ONLY public.app_notificationsentto ALTER COLUMN id SET DEFAULT nextval('public.app_notificationsentto_id_seq'::regclass);
 H   ALTER TABLE public.app_notificationsentto ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232    233            9           2604    57198    app_profile id    DEFAULT     p   ALTER TABLE ONLY public.app_profile ALTER COLUMN id SET DEFAULT nextval('public.app_profile_id_seq'::regclass);
 =   ALTER TABLE public.app_profile ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    231    230    231            8           2604    57187    app_stock id    DEFAULT     l   ALTER TABLE ONLY public.app_stock ALTER COLUMN id SET DEFAULT nextval('public.app_stock_id_seq'::regclass);
 ;   ALTER TABLE public.app_stock ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    229    229            .           2604    57006    auth_group id    DEFAULT     n   ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    209    209            /           2604    57016    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211            -           2604    56998    auth_permission id    DEFAULT     x   ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    207    207            0           2604    57024    auth_user id    DEFAULT     l   ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    213    213            1           2604    57034    auth_user_groups id    DEFAULT     z   ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214    215            2           2604    57042    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    217    217            A           2604    57437    background_task id    DEFAULT     x   ALTER TABLE ONLY public.background_task ALTER COLUMN id SET DEFAULT nextval('public.background_task_id_seq'::regclass);
 A   ALTER TABLE public.background_task ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    246    245    246            ?           2604    57425     background_task_completedtask id    DEFAULT     �   ALTER TABLE ONLY public.background_task_completedtask ALTER COLUMN id SET DEFAULT nextval('public.background_task_completedtask_id_seq'::regclass);
 O   ALTER TABLE public.background_task_completedtask ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    244    243    244            C           2604    57487    bank_account id    DEFAULT     r   ALTER TABLE ONLY public.bank_account ALTER COLUMN id SET DEFAULT nextval('public.bank_account_id_seq'::regclass);
 >   ALTER TABLE public.bank_account ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    248    247    248            E           2604    57505    bank_deposit id    DEFAULT     r   ALTER TABLE ONLY public.bank_deposit ALTER COLUMN id SET DEFAULT nextval('public.bank_deposit_id_seq'::regclass);
 >   ALTER TABLE public.bank_deposit ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    251    252    252            D           2604    57497    bank_withdraw id    DEFAULT     t   ALTER TABLE ONLY public.bank_withdraw ALTER COLUMN id SET DEFAULT nextval('public.bank_withdraw_id_seq'::regclass);
 ?   ALTER TABLE public.bank_withdraw ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    250    249    250            3           2604    57102    django_admin_log id    DEFAULT     z   ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    218    219            ,           2604    56988    django_content_type id    DEFAULT     �   ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            +           2604    56977    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            �          0    57132 	   app_batch 
   TABLE DATA           �   COPY public.app_batch (id, batch_no, batch_type, created_on, is_return_batch, state, description, miscellaneous, created_by_id, group_id) FROM stdin;
    public          postgres    false    221   \�      �          0    57145    app_documentfile 
   TABLE DATA           �   COPY public.app_documentfile (id, file_reference, created_on, state, file_barcode, flagged, lock, file_path, stage, miscellaneous, assigned_to_id, batch_id, file_created_by_id, file_type_id, original_batch_id) FROM stdin;
    public          postgres    false    223   y�      �          0    57237    app_documentfiledetail 
   TABLE DATA             COPY public.app_documentfiledetail (id, document_barcode, document_name, document_content, document_file_path, created_on, flagged, state, passed_qa, passed_validated, miscellaneous, assigned_to_id, doc_created_by_id, document_type_id, file_reference) FROM stdin;
    public          postgres    false    241   ��      �          0    57158    app_documentfiletype 
   TABLE DATA           K   COPY public.app_documentfiletype (file_type, file_description) FROM stdin;
    public          postgres    false    224   ��      �          0    57163    app_documenttype 
   TABLE DATA           e   COPY public.app_documenttype (document_name, document_field_specs, document_description) FROM stdin;
    public          postgres    false    225   ��      �          0    57229 	   app_filer 
   TABLE DATA           X   COPY public.app_filer (id, filepond, document_reference, file_reference_id) FROM stdin;
    public          postgres    false    239   ��      �          0    57221    app_modification 
   TABLE DATA           r   COPY public.app_modification (id, modified_from_stage, modified_to_stage, created_at, by_id, file_id) FROM stdin;
    public          postgres    false    237   
�      �          0    57213    app_money_disburdment 
   TABLE DATA           Q   COPY public.app_money_disburdment (id, created_at, sent_at, user_id) FROM stdin;
    public          postgres    false    235   '�      �          0    57173    app_notification 
   TABLE DATA           u   COPY public.app_notification (id, comment, created_at, resolved, created_by_id, file_id, resolved_by_id) FROM stdin;
    public          postgres    false    227   D�      �          0    57205    app_notificationsentto 
   TABLE DATA           W   COPY public.app_notificationsentto (id, read_at, notification_id, user_id) FROM stdin;
    public          postgres    false    233   a�      �          0    57195    app_profile 
   TABLE DATA           k   COPY public.app_profile (id, id_no, phone, full_name, first_login, image, elderly_id, user_id) FROM stdin;
    public          postgres    false    231   ~�      �          0    57184 	   app_stock 
   TABLE DATA           �   COPY public.app_stock (id, file_number, comment, name, nationality, cross_reference, file_category, date_last_correspondence, date_first_correspondence, location_of_file, created_at, updated_at, created_by_id) FROM stdin;
    public          postgres    false    229   ��      �          0    57003 
   auth_group 
   TABLE DATA           .   COPY public.auth_group (id, name) FROM stdin;
    public          postgres    false    209   ��      �          0    57013    auth_group_permissions 
   TABLE DATA           M   COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public          postgres    false    211   ��      �          0    56995    auth_permission 
   TABLE DATA           N   COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
    public          postgres    false    207   �      �          0    57021 	   auth_user 
   TABLE DATA           �   COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public          postgres    false    213   ��      �          0    57031    auth_user_groups 
   TABLE DATA           A   COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
    public          postgres    false    215   ��      �          0    57039    auth_user_user_permissions 
   TABLE DATA           P   COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public          postgres    false    217   ��      �          0    57402    authtoken_token 
   TABLE DATA           @   COPY public.authtoken_token (key, created, user_id) FROM stdin;
    public          postgres    false    242   ��      �          0    57434    background_task 
   TABLE DATA           �   COPY public.background_task (id, task_name, task_params, task_hash, verbose_name, priority, run_at, repeat, repeat_until, queue, attempts, failed_at, last_error, locked_by, locked_at, creator_object_id, creator_content_type_id) FROM stdin;
    public          postgres    false    246   ��      �          0    57422    background_task_completedtask 
   TABLE DATA           �   COPY public.background_task_completedtask (id, task_name, task_params, task_hash, verbose_name, priority, run_at, repeat, repeat_until, queue, attempts, failed_at, last_error, locked_by, locked_at, creator_object_id, creator_content_type_id) FROM stdin;
    public          postgres    false    244   ��      �          0    57484    bank_account 
   TABLE DATA           <   COPY public.bank_account (id, balance, user_id) FROM stdin;
    public          postgres    false    248   �      �          0    57502    bank_deposit 
   TABLE DATA           X   COPY public.bank_deposit (id, month, year, amount, balance, at, account_id) FROM stdin;
    public          postgres    false    252   4�      �          0    57494    bank_withdraw 
   TABLE DATA           e   COPY public.bank_withdraw (id, amount, balance, at, account_id, by_agent_id, by_user_id) FROM stdin;
    public          postgres    false    250   Q�      �          0    57099    django_admin_log 
   TABLE DATA           �   COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public          postgres    false    219   n�      �          0    56985    django_content_type 
   TABLE DATA           C   COPY public.django_content_type (id, app_label, model) FROM stdin;
    public          postgres    false    205   ��      �          0    56974    django_migrations 
   TABLE DATA           C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public          postgres    false    203   ��      �          0    57549    django_session 
   TABLE DATA           P   COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
    public          postgres    false    254   ��      �          0    57540    records_records 
   TABLE DATA           �   COPY public.records_records (id, first_name, last_name, residence, country, education, occupation, marital_status, bio, recorded_at) FROM stdin;
    public          postgres    false    253   ��      �           0    0    app_batch_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.app_batch_id_seq', 1, false);
          public          postgres    false    220            �           0    0    app_documentfile_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.app_documentfile_id_seq', 1, false);
          public          postgres    false    222            �           0    0    app_documentfiledetail_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.app_documentfiledetail_id_seq', 1, false);
          public          postgres    false    240            �           0    0    app_filer_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.app_filer_id_seq', 1, false);
          public          postgres    false    238            �           0    0    app_modification_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.app_modification_id_seq', 1, false);
          public          postgres    false    236            �           0    0    app_money_disburdment_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.app_money_disburdment_id_seq', 1, false);
          public          postgres    false    234            �           0    0    app_notification_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.app_notification_id_seq', 1, false);
          public          postgres    false    226            �           0    0    app_notificationsentto_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.app_notificationsentto_id_seq', 1, false);
          public          postgres    false    232            �           0    0    app_profile_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.app_profile_id_seq', 1, true);
          public          postgres    false    230            �           0    0    app_stock_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.app_stock_id_seq', 1, false);
          public          postgres    false    228            �           0    0    auth_group_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);
          public          postgres    false    208            �           0    0    auth_group_permissions_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);
          public          postgres    false    210            �           0    0    auth_permission_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_permission_id_seq', 114, true);
          public          postgres    false    206            �           0    0    auth_user_groups_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);
          public          postgres    false    214            �           0    0    auth_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);
          public          postgres    false    212            �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);
          public          postgres    false    216            �           0    0 $   background_task_completedtask_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.background_task_completedtask_id_seq', 1, false);
          public          postgres    false    243            �           0    0    background_task_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.background_task_id_seq', 1, false);
          public          postgres    false    245            �           0    0    bank_account_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.bank_account_id_seq', 1, false);
          public          postgres    false    247            �           0    0    bank_deposit_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.bank_deposit_id_seq', 1, false);
          public          postgres    false    251            �           0    0    bank_withdraw_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.bank_withdraw_id_seq', 1, false);
          public          postgres    false    249                        0    0    django_admin_log_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);
          public          postgres    false    218                       0    0    django_content_type_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.django_content_type_id_seq', 25, true);
          public          postgres    false    204                       0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 25, true);
          public          postgres    false    202            s           2606    57142     app_batch app_batch_batch_no_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.app_batch
    ADD CONSTRAINT app_batch_batch_no_key UNIQUE (batch_no);
 J   ALTER TABLE ONLY public.app_batch DROP CONSTRAINT app_batch_batch_no_key;
       public            postgres    false    221            w           2606    57140    app_batch app_batch_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.app_batch
    ADD CONSTRAINT app_batch_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.app_batch DROP CONSTRAINT app_batch_pkey;
       public            postgres    false    221            }           2606    57157 2   app_documentfile app_documentfile_file_barcode_key 
   CONSTRAINT     u   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_file_barcode_key UNIQUE (file_barcode);
 \   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_file_barcode_key;
       public            postgres    false    223            �           2606    57155 4   app_documentfile app_documentfile_file_reference_key 
   CONSTRAINT     y   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_file_reference_key UNIQUE (file_reference);
 ^   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_file_reference_key;
       public            postgres    false    223            �           2606    57153 &   app_documentfile app_documentfile_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_pkey;
       public            postgres    false    223            �           2606    57247 B   app_documentfiledetail app_documentfiledetail_document_barcode_key 
   CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledetail_document_barcode_key UNIQUE (document_barcode);
 l   ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledetail_document_barcode_key;
       public            postgres    false    241            �           2606    57245 2   app_documentfiledetail app_documentfiledetail_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledetail_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledetail_pkey;
       public            postgres    false    241            �           2606    57162 .   app_documentfiletype app_documentfiletype_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.app_documentfiletype
    ADD CONSTRAINT app_documentfiletype_pkey PRIMARY KEY (file_type);
 X   ALTER TABLE ONLY public.app_documentfiletype DROP CONSTRAINT app_documentfiletype_pkey;
       public            postgres    false    224            �           2606    57170 &   app_documenttype app_documenttype_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.app_documenttype
    ADD CONSTRAINT app_documenttype_pkey PRIMARY KEY (document_name);
 P   ALTER TABLE ONLY public.app_documenttype DROP CONSTRAINT app_documenttype_pkey;
       public            postgres    false    225            �           2606    57234    app_filer app_filer_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.app_filer
    ADD CONSTRAINT app_filer_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.app_filer DROP CONSTRAINT app_filer_pkey;
       public            postgres    false    239            �           2606    57226 &   app_modification app_modification_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.app_modification
    ADD CONSTRAINT app_modification_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.app_modification DROP CONSTRAINT app_modification_pkey;
       public            postgres    false    237            �           2606    57218 0   app_money_disburdment app_money_disburdment_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.app_money_disburdment
    ADD CONSTRAINT app_money_disburdment_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.app_money_disburdment DROP CONSTRAINT app_money_disburdment_pkey;
       public            postgres    false    235            �           2606    57181 &   app_notification app_notification_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.app_notification
    ADD CONSTRAINT app_notification_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.app_notification DROP CONSTRAINT app_notification_pkey;
       public            postgres    false    227            �           2606    57210 2   app_notificationsentto app_notificationsentto_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.app_notificationsentto
    ADD CONSTRAINT app_notificationsentto_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.app_notificationsentto DROP CONSTRAINT app_notificationsentto_pkey;
       public            postgres    false    233            �           2606    57200    app_profile app_profile_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.app_profile
    ADD CONSTRAINT app_profile_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.app_profile DROP CONSTRAINT app_profile_pkey;
       public            postgres    false    231            �           2606    57202 #   app_profile app_profile_user_id_key 
   CONSTRAINT     a   ALTER TABLE ONLY public.app_profile
    ADD CONSTRAINT app_profile_user_id_key UNIQUE (user_id);
 M   ALTER TABLE ONLY public.app_profile DROP CONSTRAINT app_profile_user_id_key;
       public            postgres    false    231            �           2606    57192    app_stock app_stock_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.app_stock
    ADD CONSTRAINT app_stock_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.app_stock DROP CONSTRAINT app_stock_pkey;
       public            postgres    false    229            S           2606    57128    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public            postgres    false    209            X           2606    57065 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public            postgres    false    211    211            [           2606    57018 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public            postgres    false    211            U           2606    57008    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public            postgres    false    209            N           2606    57051 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public            postgres    false    207    207            P           2606    57000 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public            postgres    false    207            c           2606    57036 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public            postgres    false    215            f           2606    57080 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public            postgres    false    215    215            ]           2606    57026    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public            postgres    false    213            i           2606    57044 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public            postgres    false    217            l           2606    57094 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public            postgres    false    217    217            `           2606    57122     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public            postgres    false    213            �           2606    57406 $   authtoken_token authtoken_token_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);
 N   ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_pkey;
       public            postgres    false    242            �           2606    57408 +   authtoken_token authtoken_token_user_id_key 
   CONSTRAINT     i   ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);
 U   ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_key;
       public            postgres    false    242            �           2606    57431 @   background_task_completedtask background_task_completedtask_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.background_task_completedtask
    ADD CONSTRAINT background_task_completedtask_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.background_task_completedtask DROP CONSTRAINT background_task_completedtask_pkey;
       public            postgres    false    244            �           2606    57443 $   background_task background_task_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.background_task
    ADD CONSTRAINT background_task_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.background_task DROP CONSTRAINT background_task_pkey;
       public            postgres    false    246            �           2606    57489    bank_account bank_account_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.bank_account
    ADD CONSTRAINT bank_account_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.bank_account DROP CONSTRAINT bank_account_pkey;
       public            postgres    false    248            �           2606    57491 %   bank_account bank_account_user_id_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.bank_account
    ADD CONSTRAINT bank_account_user_id_key UNIQUE (user_id);
 O   ALTER TABLE ONLY public.bank_account DROP CONSTRAINT bank_account_user_id_key;
       public            postgres    false    248            �           2606    57510    bank_deposit bank_deposit_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.bank_deposit
    ADD CONSTRAINT bank_deposit_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.bank_deposit DROP CONSTRAINT bank_deposit_pkey;
       public            postgres    false    252            �           2606    57499     bank_withdraw bank_withdraw_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.bank_withdraw
    ADD CONSTRAINT bank_withdraw_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.bank_withdraw DROP CONSTRAINT bank_withdraw_pkey;
       public            postgres    false    250            o           2606    57108 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public            postgres    false    219            I           2606    56992 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public            postgres    false    205    205            K           2606    56990 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public            postgres    false    205            G           2606    56982 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public            postgres    false    203            �           2606    57556 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public            postgres    false    254            �           2606    57547 $   records_records records_records_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.records_records
    ADD CONSTRAINT records_records_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.records_records DROP CONSTRAINT records_records_pkey;
       public            postgres    false    253            q           1259    57267     app_batch_batch_no_324f3d34_like    INDEX     n   CREATE INDEX app_batch_batch_no_324f3d34_like ON public.app_batch USING btree (batch_no varchar_pattern_ops);
 4   DROP INDEX public.app_batch_batch_no_324f3d34_like;
       public            postgres    false    221            t           1259    57268     app_batch_created_by_id_7d2025b4    INDEX     _   CREATE INDEX app_batch_created_by_id_7d2025b4 ON public.app_batch USING btree (created_by_id);
 4   DROP INDEX public.app_batch_created_by_id_7d2025b4;
       public            postgres    false    221            u           1259    57269    app_batch_group_id_338826b8    INDEX     U   CREATE INDEX app_batch_group_id_338826b8 ON public.app_batch USING btree (group_id);
 /   DROP INDEX public.app_batch_group_id_338826b8;
       public            postgres    false    221            y           1259    57287 (   app_documentfile_assigned_to_id_af10540d    INDEX     o   CREATE INDEX app_documentfile_assigned_to_id_af10540d ON public.app_documentfile USING btree (assigned_to_id);
 <   DROP INDEX public.app_documentfile_assigned_to_id_af10540d;
       public            postgres    false    223            z           1259    57288 "   app_documentfile_batch_id_c3d8c98a    INDEX     c   CREATE INDEX app_documentfile_batch_id_c3d8c98a ON public.app_documentfile USING btree (batch_id);
 6   DROP INDEX public.app_documentfile_batch_id_c3d8c98a;
       public            postgres    false    223            {           1259    57286 +   app_documentfile_file_barcode_a6ae686b_like    INDEX     �   CREATE INDEX app_documentfile_file_barcode_a6ae686b_like ON public.app_documentfile USING btree (file_barcode varchar_pattern_ops);
 ?   DROP INDEX public.app_documentfile_file_barcode_a6ae686b_like;
       public            postgres    false    223            ~           1259    57289 ,   app_documentfile_file_created_by_id_11b7a1fa    INDEX     w   CREATE INDEX app_documentfile_file_created_by_id_11b7a1fa ON public.app_documentfile USING btree (file_created_by_id);
 @   DROP INDEX public.app_documentfile_file_created_by_id_11b7a1fa;
       public            postgres    false    223                       1259    57285 -   app_documentfile_file_reference_66bc5c87_like    INDEX     �   CREATE INDEX app_documentfile_file_reference_66bc5c87_like ON public.app_documentfile USING btree (file_reference varchar_pattern_ops);
 A   DROP INDEX public.app_documentfile_file_reference_66bc5c87_like;
       public            postgres    false    223            �           1259    57389 &   app_documentfile_file_type_id_616321de    INDEX     k   CREATE INDEX app_documentfile_file_type_id_616321de ON public.app_documentfile USING btree (file_type_id);
 :   DROP INDEX public.app_documentfile_file_type_id_616321de;
       public            postgres    false    223            �           1259    57390 +   app_documentfile_file_type_id_616321de_like    INDEX     �   CREATE INDEX app_documentfile_file_type_id_616321de_like ON public.app_documentfile USING btree (file_type_id varchar_pattern_ops);
 ?   DROP INDEX public.app_documentfile_file_type_id_616321de_like;
       public            postgres    false    223            �           1259    57396 +   app_documentfile_original_batch_id_1359046c    INDEX     u   CREATE INDEX app_documentfile_original_batch_id_1359046c ON public.app_documentfile USING btree (original_batch_id);
 ?   DROP INDEX public.app_documentfile_original_batch_id_1359046c;
       public            postgres    false    223            �           1259    57384 .   app_documentfiledetail_assigned_to_id_347b22dc    INDEX     {   CREATE INDEX app_documentfiledetail_assigned_to_id_347b22dc ON public.app_documentfiledetail USING btree (assigned_to_id);
 B   DROP INDEX public.app_documentfiledetail_assigned_to_id_347b22dc;
       public            postgres    false    241            �           1259    57385 1   app_documentfiledetail_doc_created_by_id_f588a23f    INDEX     �   CREATE INDEX app_documentfiledetail_doc_created_by_id_f588a23f ON public.app_documentfiledetail USING btree (doc_created_by_id);
 E   DROP INDEX public.app_documentfiledetail_doc_created_by_id_f588a23f;
       public            postgres    false    241            �           1259    57383 5   app_documentfiledetail_document_barcode_7994eb0a_like    INDEX     �   CREATE INDEX app_documentfiledetail_document_barcode_7994eb0a_like ON public.app_documentfiledetail USING btree (document_barcode varchar_pattern_ops);
 I   DROP INDEX public.app_documentfiledetail_document_barcode_7994eb0a_like;
       public            postgres    false    241            �           1259    57386 0   app_documentfiledetail_document_type_id_7ea26f88    INDEX        CREATE INDEX app_documentfiledetail_document_type_id_7ea26f88 ON public.app_documentfiledetail USING btree (document_type_id);
 D   DROP INDEX public.app_documentfiledetail_document_type_id_7ea26f88;
       public            postgres    false    241            �           1259    57387 5   app_documentfiledetail_document_type_id_7ea26f88_like    INDEX     �   CREATE INDEX app_documentfiledetail_document_type_id_7ea26f88_like ON public.app_documentfiledetail USING btree (document_type_id varchar_pattern_ops);
 I   DROP INDEX public.app_documentfiledetail_document_type_id_7ea26f88_like;
       public            postgres    false    241            �           1259    57388 .   app_documentfiledetail_file_reference_dfe57ac8    INDEX     {   CREATE INDEX app_documentfiledetail_file_reference_dfe57ac8 ON public.app_documentfiledetail USING btree (file_reference);
 B   DROP INDEX public.app_documentfiledetail_file_reference_dfe57ac8;
       public            postgres    false    241            �           1259    57290 ,   app_documentfiletype_file_type_520cca08_like    INDEX     �   CREATE INDEX app_documentfiletype_file_type_520cca08_like ON public.app_documentfiletype USING btree (file_type varchar_pattern_ops);
 @   DROP INDEX public.app_documentfiletype_file_type_520cca08_like;
       public            postgres    false    224            �           1259    57291 ,   app_documenttype_document_name_cf3a1857_like    INDEX     �   CREATE INDEX app_documenttype_document_name_cf3a1857_like ON public.app_documenttype USING btree (document_name varchar_pattern_ops);
 @   DROP INDEX public.app_documenttype_document_name_cf3a1857_like;
       public            postgres    false    225            �           1259    57362 $   app_filer_file_reference_id_39a7730b    INDEX     g   CREATE INDEX app_filer_file_reference_id_39a7730b ON public.app_filer USING btree (file_reference_id);
 8   DROP INDEX public.app_filer_file_reference_id_39a7730b;
       public            postgres    false    239            �           1259    57355    app_modification_by_id_41a7a3c0    INDEX     ]   CREATE INDEX app_modification_by_id_41a7a3c0 ON public.app_modification USING btree (by_id);
 3   DROP INDEX public.app_modification_by_id_41a7a3c0;
       public            postgres    false    237            �           1259    57356 !   app_modification_file_id_8b2db34f    INDEX     a   CREATE INDEX app_modification_file_id_8b2db34f ON public.app_modification USING btree (file_id);
 5   DROP INDEX public.app_modification_file_id_8b2db34f;
       public            postgres    false    237            �           1259    57344 &   app_money_disburdment_user_id_c26d596f    INDEX     k   CREATE INDEX app_money_disburdment_user_id_c26d596f ON public.app_money_disburdment USING btree (user_id);
 :   DROP INDEX public.app_money_disburdment_user_id_c26d596f;
       public            postgres    false    235            �           1259    57307 '   app_notification_created_by_id_6fe2662d    INDEX     m   CREATE INDEX app_notification_created_by_id_6fe2662d ON public.app_notification USING btree (created_by_id);
 ;   DROP INDEX public.app_notification_created_by_id_6fe2662d;
       public            postgres    false    227            �           1259    57308 !   app_notification_file_id_3dee5ffc    INDEX     a   CREATE INDEX app_notification_file_id_3dee5ffc ON public.app_notification USING btree (file_id);
 5   DROP INDEX public.app_notification_file_id_3dee5ffc;
       public            postgres    false    227            �           1259    57309 (   app_notification_resolved_by_id_cafd9ecc    INDEX     o   CREATE INDEX app_notification_resolved_by_id_cafd9ecc ON public.app_notification USING btree (resolved_by_id);
 <   DROP INDEX public.app_notification_resolved_by_id_cafd9ecc;
       public            postgres    false    227            �           1259    57337 /   app_notificationsentto_notification_id_096ea88d    INDEX     }   CREATE INDEX app_notificationsentto_notification_id_096ea88d ON public.app_notificationsentto USING btree (notification_id);
 C   DROP INDEX public.app_notificationsentto_notification_id_096ea88d;
       public            postgres    false    233            �           1259    57338 '   app_notificationsentto_user_id_f62a789b    INDEX     m   CREATE INDEX app_notificationsentto_user_id_f62a789b ON public.app_notificationsentto USING btree (user_id);
 ;   DROP INDEX public.app_notificationsentto_user_id_f62a789b;
       public            postgres    false    233            �           1259    57326    app_profile_elderly_id_b2da71d2    INDEX     ]   CREATE INDEX app_profile_elderly_id_b2da71d2 ON public.app_profile USING btree (elderly_id);
 3   DROP INDEX public.app_profile_elderly_id_b2da71d2;
       public            postgres    false    231            �           1259    57315     app_stock_created_by_id_38dac542    INDEX     _   CREATE INDEX app_stock_created_by_id_38dac542 ON public.app_stock USING btree (created_by_id);
 4   DROP INDEX public.app_stock_created_by_id_38dac542;
       public            postgres    false    229            Q           1259    57129    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public            postgres    false    209            V           1259    57066 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public            postgres    false    211            Y           1259    57067 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public            postgres    false    211            L           1259    57052 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public            postgres    false    207            a           1259    57082 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public            postgres    false    215            d           1259    57081 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public            postgres    false    215            g           1259    57096 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public            postgres    false    217            j           1259    57095 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public            postgres    false    217            ^           1259    57123     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public            postgres    false    213            �           1259    57414 !   authtoken_token_key_10f0b77e_like    INDEX     p   CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);
 5   DROP INDEX public.authtoken_token_key_10f0b77e_like;
       public            postgres    false    242            �           1259    57476 !   background_task_attempts_a9ade23d    INDEX     a   CREATE INDEX background_task_attempts_a9ade23d ON public.background_task USING btree (attempts);
 5   DROP INDEX public.background_task_attempts_a9ade23d;
       public            postgres    false    246            �           1259    57457 /   background_task_completedtask_attempts_772a6783    INDEX     }   CREATE INDEX background_task_completedtask_attempts_772a6783 ON public.background_task_completedtask USING btree (attempts);
 C   DROP INDEX public.background_task_completedtask_attempts_772a6783;
       public            postgres    false    244            �           1259    57462 >   background_task_completedtask_creator_content_type_id_21d6a741    INDEX     �   CREATE INDEX background_task_completedtask_creator_content_type_id_21d6a741 ON public.background_task_completedtask USING btree (creator_content_type_id);
 R   DROP INDEX public.background_task_completedtask_creator_content_type_id_21d6a741;
       public            postgres    false    244            �           1259    57458 0   background_task_completedtask_failed_at_3de56618    INDEX        CREATE INDEX background_task_completedtask_failed_at_3de56618 ON public.background_task_completedtask USING btree (failed_at);
 D   DROP INDEX public.background_task_completedtask_failed_at_3de56618;
       public            postgres    false    244            �           1259    57461 0   background_task_completedtask_locked_at_29c62708    INDEX        CREATE INDEX background_task_completedtask_locked_at_29c62708 ON public.background_task_completedtask USING btree (locked_at);
 D   DROP INDEX public.background_task_completedtask_locked_at_29c62708;
       public            postgres    false    244            �           1259    57459 0   background_task_completedtask_locked_by_edc8a213    INDEX        CREATE INDEX background_task_completedtask_locked_by_edc8a213 ON public.background_task_completedtask USING btree (locked_by);
 D   DROP INDEX public.background_task_completedtask_locked_by_edc8a213;
       public            postgres    false    244            �           1259    57460 5   background_task_completedtask_locked_by_edc8a213_like    INDEX     �   CREATE INDEX background_task_completedtask_locked_by_edc8a213_like ON public.background_task_completedtask USING btree (locked_by varchar_pattern_ops);
 I   DROP INDEX public.background_task_completedtask_locked_by_edc8a213_like;
       public            postgres    false    244            �           1259    57453 /   background_task_completedtask_priority_9080692e    INDEX     }   CREATE INDEX background_task_completedtask_priority_9080692e ON public.background_task_completedtask USING btree (priority);
 C   DROP INDEX public.background_task_completedtask_priority_9080692e;
       public            postgres    false    244            �           1259    57455 ,   background_task_completedtask_queue_61fb0415    INDEX     w   CREATE INDEX background_task_completedtask_queue_61fb0415 ON public.background_task_completedtask USING btree (queue);
 @   DROP INDEX public.background_task_completedtask_queue_61fb0415;
       public            postgres    false    244            �           1259    57456 1   background_task_completedtask_queue_61fb0415_like    INDEX     �   CREATE INDEX background_task_completedtask_queue_61fb0415_like ON public.background_task_completedtask USING btree (queue varchar_pattern_ops);
 E   DROP INDEX public.background_task_completedtask_queue_61fb0415_like;
       public            postgres    false    244            �           1259    57454 -   background_task_completedtask_run_at_77c80f34    INDEX     y   CREATE INDEX background_task_completedtask_run_at_77c80f34 ON public.background_task_completedtask USING btree (run_at);
 A   DROP INDEX public.background_task_completedtask_run_at_77c80f34;
       public            postgres    false    244            �           1259    57451 0   background_task_completedtask_task_hash_91187576    INDEX        CREATE INDEX background_task_completedtask_task_hash_91187576 ON public.background_task_completedtask USING btree (task_hash);
 D   DROP INDEX public.background_task_completedtask_task_hash_91187576;
       public            postgres    false    244            �           1259    57452 5   background_task_completedtask_task_hash_91187576_like    INDEX     �   CREATE INDEX background_task_completedtask_task_hash_91187576_like ON public.background_task_completedtask USING btree (task_hash varchar_pattern_ops);
 I   DROP INDEX public.background_task_completedtask_task_hash_91187576_like;
       public            postgres    false    244            �           1259    57449 0   background_task_completedtask_task_name_388dabc2    INDEX        CREATE INDEX background_task_completedtask_task_name_388dabc2 ON public.background_task_completedtask USING btree (task_name);
 D   DROP INDEX public.background_task_completedtask_task_name_388dabc2;
       public            postgres    false    244            �           1259    57450 5   background_task_completedtask_task_name_388dabc2_like    INDEX     �   CREATE INDEX background_task_completedtask_task_name_388dabc2_like ON public.background_task_completedtask USING btree (task_name varchar_pattern_ops);
 I   DROP INDEX public.background_task_completedtask_task_name_388dabc2_like;
       public            postgres    false    244            �           1259    57481 0   background_task_creator_content_type_id_61cc9af3    INDEX        CREATE INDEX background_task_creator_content_type_id_61cc9af3 ON public.background_task USING btree (creator_content_type_id);
 D   DROP INDEX public.background_task_creator_content_type_id_61cc9af3;
       public            postgres    false    246            �           1259    57477 "   background_task_failed_at_b81bba14    INDEX     c   CREATE INDEX background_task_failed_at_b81bba14 ON public.background_task USING btree (failed_at);
 6   DROP INDEX public.background_task_failed_at_b81bba14;
       public            postgres    false    246            �           1259    57480 "   background_task_locked_at_0fb0f225    INDEX     c   CREATE INDEX background_task_locked_at_0fb0f225 ON public.background_task USING btree (locked_at);
 6   DROP INDEX public.background_task_locked_at_0fb0f225;
       public            postgres    false    246            �           1259    57478 "   background_task_locked_by_db7779e3    INDEX     c   CREATE INDEX background_task_locked_by_db7779e3 ON public.background_task USING btree (locked_by);
 6   DROP INDEX public.background_task_locked_by_db7779e3;
       public            postgres    false    246            �           1259    57479 '   background_task_locked_by_db7779e3_like    INDEX     |   CREATE INDEX background_task_locked_by_db7779e3_like ON public.background_task USING btree (locked_by varchar_pattern_ops);
 ;   DROP INDEX public.background_task_locked_by_db7779e3_like;
       public            postgres    false    246            �           1259    57472 !   background_task_priority_88bdbce9    INDEX     a   CREATE INDEX background_task_priority_88bdbce9 ON public.background_task USING btree (priority);
 5   DROP INDEX public.background_task_priority_88bdbce9;
       public            postgres    false    246            �           1259    57474    background_task_queue_1d5f3a40    INDEX     [   CREATE INDEX background_task_queue_1d5f3a40 ON public.background_task USING btree (queue);
 2   DROP INDEX public.background_task_queue_1d5f3a40;
       public            postgres    false    246            �           1259    57475 #   background_task_queue_1d5f3a40_like    INDEX     t   CREATE INDEX background_task_queue_1d5f3a40_like ON public.background_task USING btree (queue varchar_pattern_ops);
 7   DROP INDEX public.background_task_queue_1d5f3a40_like;
       public            postgres    false    246            �           1259    57473    background_task_run_at_7baca3aa    INDEX     ]   CREATE INDEX background_task_run_at_7baca3aa ON public.background_task USING btree (run_at);
 3   DROP INDEX public.background_task_run_at_7baca3aa;
       public            postgres    false    246            �           1259    57470 "   background_task_task_hash_d8f233bd    INDEX     c   CREATE INDEX background_task_task_hash_d8f233bd ON public.background_task USING btree (task_hash);
 6   DROP INDEX public.background_task_task_hash_d8f233bd;
       public            postgres    false    246            �           1259    57471 '   background_task_task_hash_d8f233bd_like    INDEX     |   CREATE INDEX background_task_task_hash_d8f233bd_like ON public.background_task USING btree (task_hash varchar_pattern_ops);
 ;   DROP INDEX public.background_task_task_hash_d8f233bd_like;
       public            postgres    false    246            �           1259    57468 "   background_task_task_name_4562d56a    INDEX     c   CREATE INDEX background_task_task_name_4562d56a ON public.background_task USING btree (task_name);
 6   DROP INDEX public.background_task_task_name_4562d56a;
       public            postgres    false    246            �           1259    57469 '   background_task_task_name_4562d56a_like    INDEX     |   CREATE INDEX background_task_task_name_4562d56a_like ON public.background_task USING btree (task_name varchar_pattern_ops);
 ;   DROP INDEX public.background_task_task_name_4562d56a_like;
       public            postgres    false    246            �           1259    57539     bank_deposit_account_id_f12e05c8    INDEX     _   CREATE INDEX bank_deposit_account_id_f12e05c8 ON public.bank_deposit USING btree (account_id);
 4   DROP INDEX public.bank_deposit_account_id_f12e05c8;
       public            postgres    false    252            �           1259    57531 !   bank_withdraw_account_id_90304459    INDEX     a   CREATE INDEX bank_withdraw_account_id_90304459 ON public.bank_withdraw USING btree (account_id);
 5   DROP INDEX public.bank_withdraw_account_id_90304459;
       public            postgres    false    250            �           1259    57532 "   bank_withdraw_by_agent_id_017fa5ab    INDEX     c   CREATE INDEX bank_withdraw_by_agent_id_017fa5ab ON public.bank_withdraw USING btree (by_agent_id);
 6   DROP INDEX public.bank_withdraw_by_agent_id_017fa5ab;
       public            postgres    false    250            �           1259    57533 !   bank_withdraw_by_user_id_998d5848    INDEX     a   CREATE INDEX bank_withdraw_by_user_id_998d5848 ON public.bank_withdraw USING btree (by_user_id);
 5   DROP INDEX public.bank_withdraw_by_user_id_998d5848;
       public            postgres    false    250            x           1259    57256    batch_batch_no_idx    INDEX     K   CREATE INDEX batch_batch_no_idx ON public.app_batch USING brin (batch_no);
 &   DROP INDEX public.batch_batch_no_idx;
       public            postgres    false    221            m           1259    57119 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public            postgres    false    219            p           1259    57120 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public            postgres    false    219            �           1259    57558 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public            postgres    false    254            �           1259    57557 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public            postgres    false    254            �           1259    57252    doc_doc_barcode_idx    INDEX     a   CREATE INDEX doc_doc_barcode_idx ON public.app_documentfiledetail USING brin (document_barcode);
 '   DROP INDEX public.doc_doc_barcode_idx;
       public            postgres    false    241            �           1259    57254    file_file_barcode_idx    INDEX     Y   CREATE INDEX file_file_barcode_idx ON public.app_documentfile USING brin (file_barcode);
 )   DROP INDEX public.file_file_barcode_idx;
       public            postgres    false    223            �           1259    57253    file_file_reference_idx    INDEX     ]   CREATE INDEX file_file_reference_idx ON public.app_documentfile USING brin (file_reference);
 +   DROP INDEX public.file_file_reference_idx;
       public            postgres    false    223            �           1259    57255    file_stage_idx    INDEX     K   CREATE INDEX file_stage_idx ON public.app_documentfile USING brin (stage);
 "   DROP INDEX public.file_stage_idx;
       public            postgres    false    223            �           1259    57251    modification_from_to__indx    INDEX     x   CREATE INDEX modification_from_to__indx ON public.app_modification USING brin (modified_from_stage, modified_to_stage);
 .   DROP INDEX public.modification_from_to__indx;
       public            postgres    false    237    237            �           1259    57548     records_records_id_68772ce5_like    INDEX     n   CREATE INDEX records_records_id_68772ce5_like ON public.records_records USING btree (id varchar_pattern_ops);
 4   DROP INDEX public.records_records_id_68772ce5_like;
       public            postgres    false    253            �           1259    57248    stock_file_no_indx    INDEX     N   CREATE INDEX stock_file_no_indx ON public.app_stock USING brin (file_number);
 &   DROP INDEX public.stock_file_no_indx;
       public            postgres    false    229            �           1259    57250    stock_location_indx    INDEX     T   CREATE INDEX stock_location_indx ON public.app_stock USING brin (location_of_file);
 '   DROP INDEX public.stock_location_indx;
       public            postgres    false    229            �           1259    57249    stock_name_indx    INDEX     D   CREATE INDEX stock_name_indx ON public.app_stock USING brin (name);
 #   DROP INDEX public.stock_name_indx;
       public            postgres    false    229            �           2606    57257 :   app_batch app_batch_created_by_id_7d2025b4_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_batch
    ADD CONSTRAINT app_batch_created_by_id_7d2025b4_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 d   ALTER TABLE ONLY public.app_batch DROP CONSTRAINT app_batch_created_by_id_7d2025b4_fk_auth_user_id;
       public          postgres    false    2909    213    221            �           2606    57262 6   app_batch app_batch_group_id_338826b8_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_batch
    ADD CONSTRAINT app_batch_group_id_338826b8_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 `   ALTER TABLE ONLY public.app_batch DROP CONSTRAINT app_batch_group_id_338826b8_fk_auth_group_id;
       public          postgres    false    2901    209    221            �           2606    57270 I   app_documentfile app_documentfile_assigned_to_id_af10540d_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_assigned_to_id_af10540d_fk_auth_user_id FOREIGN KEY (assigned_to_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_assigned_to_id_af10540d_fk_auth_user_id;
       public          postgres    false    223    2909    213            �           2606    57275 C   app_documentfile app_documentfile_batch_id_c3d8c98a_fk_app_batch_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_batch_id_c3d8c98a_fk_app_batch_id FOREIGN KEY (batch_id) REFERENCES public.app_batch(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_batch_id_c3d8c98a_fk_app_batch_id;
       public          postgres    false    223    221    2935            �           2606    57280 M   app_documentfile app_documentfile_file_created_by_id_11b7a1fa_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_file_created_by_id_11b7a1fa_fk_auth_user_id FOREIGN KEY (file_created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 w   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_file_created_by_id_11b7a1fa_fk_auth_user_id;
       public          postgres    false    223    213    2909                        2606    57391 D   app_documentfile app_documentfile_file_type_id_616321de_fk_app_docum    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_file_type_id_616321de_fk_app_docum FOREIGN KEY (file_type_id) REFERENCES public.app_documentfiletype(file_type) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_file_type_id_616321de_fk_app_docum;
       public          postgres    false    224    223    2956                       2606    57397 L   app_documentfile app_documentfile_original_batch_id_1359046c_fk_app_batch_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfile
    ADD CONSTRAINT app_documentfile_original_batch_id_1359046c_fk_app_batch_id FOREIGN KEY (original_batch_id) REFERENCES public.app_batch(id) DEFERRABLE INITIALLY DEFERRED;
 v   ALTER TABLE ONLY public.app_documentfile DROP CONSTRAINT app_documentfile_original_batch_id_1359046c_fk_app_batch_id;
       public          postgres    false    221    2935    223                       2606    57368 S   app_documentfiledetail app_documentfiledeta_doc_created_by_id_f588a23f_fk_auth_user    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledeta_doc_created_by_id_f588a23f_fk_auth_user FOREIGN KEY (doc_created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledeta_doc_created_by_id_f588a23f_fk_auth_user;
       public          postgres    false    241    2909    213                       2606    57373 R   app_documentfiledetail app_documentfiledeta_document_type_id_7ea26f88_fk_app_docum    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledeta_document_type_id_7ea26f88_fk_app_docum FOREIGN KEY (document_type_id) REFERENCES public.app_documenttype(document_name) DEFERRABLE INITIALLY DEFERRED;
 |   ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledeta_document_type_id_7ea26f88_fk_app_docum;
       public          postgres    false    241    225    2959                       2606    57378 P   app_documentfiledetail app_documentfiledeta_file_reference_dfe57ac8_fk_app_docum    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledeta_file_reference_dfe57ac8_fk_app_docum FOREIGN KEY (file_reference) REFERENCES public.app_documentfile(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledeta_file_reference_dfe57ac8_fk_app_docum;
       public          postgres    false    2950    223    241                       2606    57363 U   app_documentfiledetail app_documentfiledetail_assigned_to_id_347b22dc_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_documentfiledetail
    ADD CONSTRAINT app_documentfiledetail_assigned_to_id_347b22dc_fk_auth_user_id FOREIGN KEY (assigned_to_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
    ALTER TABLE ONLY public.app_documentfiledetail DROP CONSTRAINT app_documentfiledetail_assigned_to_id_347b22dc_fk_auth_user_id;
       public          postgres    false    241    2909    213                       2606    57357 E   app_filer app_filer_file_reference_id_39a7730b_fk_app_documentfile_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_filer
    ADD CONSTRAINT app_filer_file_reference_id_39a7730b_fk_app_documentfile_id FOREIGN KEY (file_reference_id) REFERENCES public.app_documentfile(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.app_filer DROP CONSTRAINT app_filer_file_reference_id_39a7730b_fk_app_documentfile_id;
       public          postgres    false    223    2950    239                       2606    57345 @   app_modification app_modification_by_id_41a7a3c0_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_modification
    ADD CONSTRAINT app_modification_by_id_41a7a3c0_fk_auth_user_id FOREIGN KEY (by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.app_modification DROP CONSTRAINT app_modification_by_id_41a7a3c0_fk_auth_user_id;
       public          postgres    false    213    237    2909                       2606    57350 I   app_modification app_modification_file_id_8b2db34f_fk_app_documentfile_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_modification
    ADD CONSTRAINT app_modification_file_id_8b2db34f_fk_app_documentfile_id FOREIGN KEY (file_id) REFERENCES public.app_documentfile(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.app_modification DROP CONSTRAINT app_modification_file_id_8b2db34f_fk_app_documentfile_id;
       public          postgres    false    2950    237    223            
           2606    57339 L   app_money_disburdment app_money_disburdment_user_id_c26d596f_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_money_disburdment
    ADD CONSTRAINT app_money_disburdment_user_id_c26d596f_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 v   ALTER TABLE ONLY public.app_money_disburdment DROP CONSTRAINT app_money_disburdment_user_id_c26d596f_fk_auth_user_id;
       public          postgres    false    235    213    2909                       2606    57292 H   app_notification app_notification_created_by_id_6fe2662d_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_notification
    ADD CONSTRAINT app_notification_created_by_id_6fe2662d_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY public.app_notification DROP CONSTRAINT app_notification_created_by_id_6fe2662d_fk_auth_user_id;
       public          postgres    false    227    213    2909                       2606    57297 I   app_notification app_notification_file_id_3dee5ffc_fk_app_documentfile_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_notification
    ADD CONSTRAINT app_notification_file_id_3dee5ffc_fk_app_documentfile_id FOREIGN KEY (file_id) REFERENCES public.app_documentfile(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.app_notification DROP CONSTRAINT app_notification_file_id_3dee5ffc_fk_app_documentfile_id;
       public          postgres    false    2950    227    223                       2606    57302 I   app_notification app_notification_resolved_by_id_cafd9ecc_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_notification
    ADD CONSTRAINT app_notification_resolved_by_id_cafd9ecc_fk_auth_user_id FOREIGN KEY (resolved_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.app_notification DROP CONSTRAINT app_notification_resolved_by_id_cafd9ecc_fk_auth_user_id;
       public          postgres    false    227    213    2909                       2606    57327 Q   app_notificationsentto app_notificationsent_notification_id_096ea88d_fk_app_notif    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_notificationsentto
    ADD CONSTRAINT app_notificationsent_notification_id_096ea88d_fk_app_notif FOREIGN KEY (notification_id) REFERENCES public.app_notification(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY public.app_notificationsentto DROP CONSTRAINT app_notificationsent_notification_id_096ea88d_fk_app_notif;
       public          postgres    false    2963    227    233            	           2606    57332 N   app_notificationsentto app_notificationsentto_user_id_f62a789b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_notificationsentto
    ADD CONSTRAINT app_notificationsentto_user_id_f62a789b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 x   ALTER TABLE ONLY public.app_notificationsentto DROP CONSTRAINT app_notificationsentto_user_id_f62a789b_fk_auth_user_id;
       public          postgres    false    2909    213    233                       2606    57316 ;   app_profile app_profile_elderly_id_b2da71d2_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_profile
    ADD CONSTRAINT app_profile_elderly_id_b2da71d2_fk_auth_user_id FOREIGN KEY (elderly_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 e   ALTER TABLE ONLY public.app_profile DROP CONSTRAINT app_profile_elderly_id_b2da71d2_fk_auth_user_id;
       public          postgres    false    231    2909    213                       2606    57321 8   app_profile app_profile_user_id_87d292a0_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_profile
    ADD CONSTRAINT app_profile_user_id_87d292a0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 b   ALTER TABLE ONLY public.app_profile DROP CONSTRAINT app_profile_user_id_87d292a0_fk_auth_user_id;
       public          postgres    false    2909    231    213                       2606    57310 :   app_stock app_stock_created_by_id_38dac542_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.app_stock
    ADD CONSTRAINT app_stock_created_by_id_38dac542_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 d   ALTER TABLE ONLY public.app_stock DROP CONSTRAINT app_stock_created_by_id_38dac542_fk_auth_user_id;
       public          postgres    false    2909    229    213            �           2606    57059 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public          postgres    false    2896    207    211            �           2606    57054 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public          postgres    false    211    2901    209            �           2606    57045 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public          postgres    false    207    2891    205            �           2606    57074 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public          postgres    false    2901    215    209            �           2606    57069 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public          postgres    false    213    2909    215            �           2606    57088 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public          postgres    false    217    207    2896            �           2606    57083 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public          postgres    false    213    217    2909                       2606    57415 @   authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id;
       public          postgres    false    242    213    2909                       2606    57444 ]   background_task_completedtask background_task_comp_creator_content_type_21d6a741_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.background_task_completedtask
    ADD CONSTRAINT background_task_comp_creator_content_type_21d6a741_fk_django_co FOREIGN KEY (creator_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.background_task_completedtask DROP CONSTRAINT background_task_comp_creator_content_type_21d6a741_fk_django_co;
       public          postgres    false    205    244    2891                       2606    57463 J   background_task background_task_creator_content_type_61cc9af3_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.background_task
    ADD CONSTRAINT background_task_creator_content_type_61cc9af3_fk_django_co FOREIGN KEY (creator_content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 t   ALTER TABLE ONLY public.background_task DROP CONSTRAINT background_task_creator_content_type_61cc9af3_fk_django_co;
       public          postgres    false    246    205    2891                       2606    57511 :   bank_account bank_account_user_id_44865741_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.bank_account
    ADD CONSTRAINT bank_account_user_id_44865741_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 d   ALTER TABLE ONLY public.bank_account DROP CONSTRAINT bank_account_user_id_44865741_fk_auth_user_id;
       public          postgres    false    213    248    2909                       2606    57534 @   bank_deposit bank_deposit_account_id_f12e05c8_fk_bank_account_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.bank_deposit
    ADD CONSTRAINT bank_deposit_account_id_f12e05c8_fk_bank_account_id FOREIGN KEY (account_id) REFERENCES public.bank_account(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.bank_deposit DROP CONSTRAINT bank_deposit_account_id_f12e05c8_fk_bank_account_id;
       public          postgres    false    248    3040    252                       2606    57516 B   bank_withdraw bank_withdraw_account_id_90304459_fk_bank_account_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.bank_withdraw
    ADD CONSTRAINT bank_withdraw_account_id_90304459_fk_bank_account_id FOREIGN KEY (account_id) REFERENCES public.bank_account(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.bank_withdraw DROP CONSTRAINT bank_withdraw_account_id_90304459_fk_bank_account_id;
       public          postgres    false    3040    250    248                       2606    57521 @   bank_withdraw bank_withdraw_by_agent_id_017fa5ab_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.bank_withdraw
    ADD CONSTRAINT bank_withdraw_by_agent_id_017fa5ab_fk_auth_user_id FOREIGN KEY (by_agent_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.bank_withdraw DROP CONSTRAINT bank_withdraw_by_agent_id_017fa5ab_fk_auth_user_id;
       public          postgres    false    2909    250    213                       2606    57526 ?   bank_withdraw bank_withdraw_by_user_id_998d5848_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.bank_withdraw
    ADD CONSTRAINT bank_withdraw_by_user_id_998d5848_fk_auth_user_id FOREIGN KEY (by_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 i   ALTER TABLE ONLY public.bank_withdraw DROP CONSTRAINT bank_withdraw_by_user_id_998d5848_fk_auth_user_id;
       public          postgres    false    2909    250    213            �           2606    57109 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public          postgres    false    219    205    2891            �           2606    57114 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public          postgres    false    219    213    2909            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   #   x�3��ΔԴ�Ҝ���t��!W� ���      �      x������ � �      �      x������ � �      �      x������ � �      �   �  x�u��n�8���S�	�P�d��6���=��@�H�Cؖ��� o�"g����K`r~_��)�}5]n�6?���v���ln?���TE@�W����+�� ��d'	BW� �9�.0ߑ�*:�������]Vk�C�ܜ �c�ܟ`� cwɢ �G������_/�&��~��(����!h�"Y! ���&:��v��`��T���c���#��P��1���6����4�Z>}\lV��{��q#	~�}-x����!�\���`ut=ZX�:Ɩ*�~O�����	C{c{=A�UD�N6S�m���[b�S� "�5!���	0���=�q�C����~�=؋��!�m��z�s��N6�����Fs�K�����T`NR�I�-����K̴������IH3t=K��F3���|�y?��7��	A),��9ȹ�� ���`��ܳ�wS�E�á�a�~��k�D�7lS�Y.8��5g���n�[Y<��_@���"H�$	o�d�m�A0�_���jf�/�I��Iـd��		D��b]?�ט��	1�S�'��ѿPT��R���B'�K>��t�
��7�L�%�ڈjJ S`JQJ��J
L��(PF!�����NT�����<Ͻ`�|�B�t�]�>�W86U%�.���8qg1�����b�+��ُ1�vj�p��}؍O��.|�Zg:��ꍏ>Z�b������__�}����Vu�����o�>������Wu}or��~$��ޮ����I����꛽J��h;����E��:��@f�/7���U!���ϼU-���e�UeHsE�aY��ט`�S�R�&�WT2#���pE��<���t0��2���P[q(#�F�22h�;��f0���Fj�d�)ӷ����xE��ܢ���{�������yO}s��6U��U��ix)`S���g&9q���#�N3���ȤSk���Λ���j/N(��U��id��;y�
�"���Ϗ��8^?�cF�/�y���_��Q�KB?_[7��@��m�O����v0�YO j���x�a��S�I�2�l�9�LT���^���s=6��]1���mA� ��A����T�~�G;��>���-� Q�"�~b,"����|��6��x̊?���w���x)���R��:^���Pi���g�T��܌���4���FM) �J ��>)���#LP      �   �   x�3�,H�NI3�/�H425S145 ��Ă�4'c_#m��R��$G��b'���Dw�(�BӤ���Hs���̼ g�|o[�?��Ĕ��<NN� &���s�2%�FF��F��f
FV��V�zf�ff����\1z\\\ ��)       �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x�e��n� E���L�&��/�*
n��`����}	�R��������V�/!��1J^�([dR	��E8vr�T]��r��ɚ�?�c��w �ʦ��V�?-���R+n�pޡf��xg��Hn�Y�ܵi�����D�Ԭ����͋@��e�������6�O��E�`�̠�#x��*����}�9Թ㬬sT���]?�L>�'Ӂ�DjB߹ЌQ��PSܼ�B��<ׅ/��	�������� �����      �     x���ݎ�0���S���h<��}�J�n�
`j�j����qR-�7p�9�9����C�f, �(��`u[  y|#��;)�Q��,�ʟ@OX�)|+��2)h�������'	�KP������s�F��N��g����zgD��u��WZ}Q���uL`��#�%��nn�*=Q���>J"�P2�J��x5��qL��^u�K���ķ�Q���eF�5��f:m�CJ)�Ά�La��t���!<���@̝ �����1��m����_� ���)"��w�>�;�RhO [�Ƚ�r��[����8o�x���8�˓ŋ\DB3��M� f���B�eR�-��P��P|CXQ��5Ln"C�4Ĉ����=4�I>%٬�:�ѯ]���@.S��w5�GBy�1?�L����r �B��.�I�6����kʪoT���� �r��=�݆�
�"�]PZ�\@A���@�_i��H��R �긣��f�7����AU��r�ɋѬ%8�Ř���:�N��y�      �      x������ � �      �      x������ � �     