# Wazee Funds


#Introduction
Face detection and facial recognition to help manage elderly funds

General Requirements

    •	Python version: 3.6+
    •	Django version: 2.2
    •	Python OpenCV version: 4.4.0.46
    •	Python OpenCv Contrib Version: 4.0.0.21
    •	Postgres Database
    
#Intallation


DataBase setting

       Create database name 'elderly' under pgAdmin

Install Dependencies via pip (requires internet)


    pip install -r requirements 



Run -

     
    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver


