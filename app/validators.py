from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def start_with_B_validator(batch_no):
    if not str(batch_no).startswith('B'):
        raise ValidationError(_("Batch number should start with 'B'"))
def start_with_A_validator(barcode):
    if not str(barcode).startswith('A'):
        raise ValidationError(_("File barcode should start with 'A'"))
def start_with_C_validator(barcode):
    if not str(barcode).startswith('C'):
        raise ValidationError(_("Document barcode should start with 'C'"))