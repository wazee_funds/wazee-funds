from django import template
from app.models import Notification

register = template.Library()


@register.filter
def pending_tickets(all_messages=Notification.objects.filter(resolved=False)):

    return all_messages.count()
