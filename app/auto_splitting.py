from django.shortcuts import render
from background_task import background
from .models import Filer, DocumentFile
import queue
import time
import os
import subprocess
from PyPDF2 import PdfFileReader, PdfFileWriter

q = queue.Queue()
standadizer_q  = queue.Queue
page_number = 0

@background(schedule=1)
def split_pdfs(file_pk):
    files = Filer.objects.filter(file_reference_id=file_pk).last().values_list('filepond', flat=True)
    start_time = time.time()
    completed = []
    for file in files:
        q.put(file)

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        # inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))
        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except Exception as exc:
            print(exc)
            continue


    context = {'files': completed}

    return 'Ok'


