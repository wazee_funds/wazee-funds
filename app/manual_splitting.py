from django.shortcuts import render

from .models import Filer, DocumentFile
import concurrent.futures
import queue
import time
import os
import subprocess
from PyPDF2 import PdfFileReader, PdfFileWriter

q = queue.Queue()
standadizer_q = queue.Queue
page_number = 0





def clean_up_split_files(file_reference):
    file = Filer.objects.filter(file_reference_id=file_reference).values_list('filepond', flat=True)
    directory = "media/" + os.path.dirname(file[0])
    for file in os.listdir(directory):
        basename = os.path.basename(file)
        file_name = os.path.splitext(basename)[0]
        print(file_name)
        if file_name.isdigit():
            os.remove(directory + file)


def split_pdfs_by_batch(request, batch_id):
    batch_files = DocumentFile.objects.filter(batch_id=batch_id).values_list('id', flat=True)

    for batch_file in batch_files:
        files = Filer.objects.filter(file_reference_id=batch_file).values_list('filepond', flat=True)
        for file in files:
            q.put(file)

    start_time = time.time()
    completed = []

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        # inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))
        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except:
            continue

    context = {'files': completed}

    return render(request, 'manual_splitting/index.html', context=context)


def split_all_pdfs(request):
    files = Filer.objects.order_by('-id').distinct('file_reference').values_list('filepond', flat=True)
    start_time = time.time()
    completed = []
    for file in files:
        q.put(file)

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        # inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))
        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except:
            continue

    context = {'files': completed}

    return render(request, 'manual_splitting/index.html', context=context)


def pdf_a_1b_standardizer(request):
    all_pdfs = Filer.objects.all().values_list('file_pond', flat=True)
    for pdf in all_pdfs:
        standadizer_q.put(pdf)

    while not standadizer_q.empty():
        shell_command = "gs -dPDFA -dBATCH -dNOPAUSE -sColorConversionStrategy=UseDeviceIndependentColor " \
                        "-sDEVICE=pdfwrite -dPDFACompatibilityPolicy=2 -sOutputFile=output_filename.pdf " \
                        "input_filename.pdf "
        subprocess.run(shell_command, shell=True)
def all_files():
    file = Filer.objects.all().values_list('filepond',flat=True)
    directory = "media/" + os.path.dirname(file[0])
    print(directory)
    for file in os.listdir(directory):
        basename = os.path.basename(file)
        file_name = os.path.splitext(basename)[0]
        print(file_name)
        if file_name.isdigit():
            print("=============> deleting ...")
            os.remove(directory + file)
            print("={}==============>deleted".format(directory + file))

def split_pdfs( file_pk):
    files = Filer.objects.filter(file_reference_id=file_pk).values_list('filepond', flat=True).last()
    print('splitting filepath================>{}'.format(files))
    start_time = time.time()
    completed = []
    for file in files:
        q.put(file)

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        # inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))
        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except Exception as exc:
            print(exc)
            continue

    context = {'files': completed}

    return

def split_only_last():
    files = Filer.objects.order_by('file_reference').distinct('file_reference').values_list('file_reference', flat=True)
    files_as_list = list(files)
    print(files_as_list)
    for file_pk in files_as_list:
        split_pdfs(None,file_pk=file_pk)
    return 'All files Ddone'

def all_files():
    clean_queue = queue.Queue()
    files = Filer.objects.all().values_list('filepond',flat=True)
    for file in files:
        clean_queue.put(file)

    while not clean_queue.empty():
        file = clean_queue.get()
        directory = "media/" + os.path.dirname(file)
        for file in os.listdir(directory):
            basename = os.path.basename(file)
            file_name = os.path.splitext(basename)[0]
            if file_name.isdigit():
                os.remove(directory + "/" +file)
                print("={}==============> deleted ".format(directory +"/" + file))


def split_pdfs( file_pk):
    files = Filer.objects.filter(file_reference_id=file_pk).values_list('filepond', flat=True).last()
    start_time = time.time()
    completed = []
    for file in files:
        q.put(file)

    while not q.empty():
        doc_pdf = q.get()

        directory = os.path.dirname(doc_pdf)

        # inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))
        try:
            inputpdf = PdfFileReader(open("media/" + doc_pdf, "rb"))

            completed.append(doc_pdf)

            for i in range(inputpdf.getNumPages()):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))

                with open("media/" + directory + "/%s.pdf" % i, "wb") as outputStream:
                    output.write(outputStream)  #
                print('================>{}==========>{}.pdf'.format(doc_pdf, i))
        except Exception as exc:
            print(exc)
            continue

    context = {'files': completed}

    return