
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import Group
from django.shortcuts import redirect, render
from django_filters.views import FilterView
from django.http import HttpResponseRedirect
from django_tables2 import SingleTableMixin, RequestConfig
from django.urls import reverse, reverse_lazy

from app.forms import BatchCreationForm, AddFileToBatchFormSet
from app.models import (Batch, DocumentFile, DocumentFileDetail,
                        STATES , STAGES, BATCH,
                        PREPARE_DIGITIZATION,PREPARE_DIGITIZATION_INTERMEDIARY,
                        AWAITING_DIGITIZATION,PREPARE_FLAGGING,
                        PREPARE_FLAGGING_INTERMEDIARY,FLAGGED,FLAGGED_AND_CORRECTED,FLAGGED_AND_CORRECTED_INTERMEDIARY,
                        SCANNED,DIGITIZED,DIGITIZED_INTERMEDIARY,RETURN_ON_DIGITIZATION,DIGITIZED_AND_ACCEPTED,DIGITIZED_AND_REJECTED,
                        ERRORS_FIXED)
from app.tables import BatchTable, DocumentFileTable, BatchDocumentTable, DocumentTable, BatchFileTable, \
    ReturnBatchTable,AdminBatchTable
from app.filters import BatchFilter, DocumentFileFilter, DocumentFilter
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView, UpdateView,
    DeleteView
)


# TODO remove restriction of quering batch
class BatchListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_batch'
    table_class = BatchTable
    template_name = 'batch/index.html'
    filterset_class = BatchFilter

    def get_queryset(self):
        batch_type=self.kwargs['batch_type']
        queryset = None
        group=Batch.get_registry_group()
        if self.request.user.is_superuser:
            pass

        elif self.request.user.has_perm('app.can_register_batch'):
               group=Batch.get_registry_group()
        elif self.request.user.has_perm('app.can_receive_file'):
            group=Batch.get_reception_group()
        if batch_type == 'ALL' and self.request.user.is_superuser:
            queryset=Batch.objects.all()
        else:
            queryset = Batch.objects.filter(batch_type=batch_type,group=group)


        self.table = BatchTable(queryset)

        if self.request.user.is_superuser:
            queryset=Batch.objects.all()
            self.table =AdminBatchTable(queryset)
        self.filter = BatchFilter(self.request.GET,
                                  queryset)
        self.table = BatchTable(self.filter.qs)
        if self.request.user.is_superuser:
            self.table =AdminBatchTable(queryset)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)
        # return Batch.objects.filter(is_return_batch=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


class ReturnBatchListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_batch'
    table_class = ReturnBatchTable
    template_name = 'batch/return_batch.html'
    filterset_class = BatchFilter

    def get_queryset(self):
        queryset = Batch.objects.filter(is_return_batch=True)
        self.table = BatchTable(queryset)
        self.filter = BatchFilter(self.request.GET,
                                  Batch.objects.filter(is_return_batch=True))
        self.table = BatchTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)
        # return Batch.objects.filter(is_return_batch=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


@login_required
def create_batch(request,batch_type):
    if request.method == 'POST':
        form = BatchCreationForm(data=request.POST)

        if form.is_valid():
            try:
                group=None

                if ((batch_type ==PREPARE_DIGITIZATION) and request.user.has_perm('app.can_register_batch')):






                    group=Batch.get_registry_group()

                elif (batch_type==PREPARE_FLAGGING and request.user.has_perm('app.can_receive_file')):
                    group = Batch.get_reception_group()







                if group:
                    batch = Batch.objects.create(batch_no=form.cleaned_data.get('batch_no'),
                                                 description=form.cleaned_data.get('description'),
                                                 created_by=request.user,
                                                 batch_type=batch_type,
                                                 group=group,
                                                 is_return_batch=False)
                    batch.save()
                    #sql_logger("Create A Batch")
                    messages.success(request, f" Batch Created successfully")

                    return redirect(reverse('batch_files', kwargs={'pk': batch.id}))
                else:
                    messages.error(request, ' something wrong happened while adding batch')
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


            except AttributeError as e:

                messages.error(request, ' something wrong happened while adding batch')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = BatchCreationForm()
    return render(request, 'batch/create.html', {'form': form})


class BatchUpdateView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, UpdateView):
    model = Batch
    fields = ['batch_no', 'description']
    template_name = 'batch/create.html'
    success_message = 'Batch updated successfully'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def test_func(self):
        batch = self.get_object()
        if self.request.user.is_superuser:
            return True
        if self.request.user.has_perm('app.can_register_batch') and batch.batch_type == 'PREPARE_DIGITIZATION':
            return True
        if self.request.user.has_perm('app.can_receive_file') and batch.batch_type == 'PREPARE_FLAGGING':
            return True

        return False

    def get_success_url(self):
        batch_type = self.get_object().batch_type
        return reverse('batch_type_index', kwargs={'batch_type': batch_type})


class BatchDeleteView(LoginRequiredMixin, SuccessMessageMixin, UserPassesTestMixin, DeleteView):
    model = Batch
    success_message = 'Batch Deleted Successfully'
    template_name = 'batch/delete_confirm.html'
    def get_context_data(self, **kwargs):

        context = super().get_context_data()
        context['batch_type'] = self.get_object().batch_type
        return context

    def test_func(self):
        batch=self.get_object()
        if self.request.user.is_superuser:
            return True
        if self.request.user.has_perm('app.can_register_batch') and batch.batch_type == 'PREPARE_DIGITIZATION':
            return True
        if self.request.user.has_perm('app.can_receive_file') and batch.batch_type == 'PREPARE_FLAGGING':
            return True

        return False



    def get_success_url(self):
        batch_type =self.get_object().batch_type
        return reverse('batch_type_index',kwargs={'batch_type': batch_type})


class BatchFilesView(LoginRequiredMixin, SingleTableMixin, FilterView):
    template_name = 'batch/filetable.html'
    table_class = BatchFileTable
    filterset_class = DocumentFileFilter

    def get_queryset(self):

        queryset=DocumentFile.objects.none()
        if self.request.user.has_perm('app.can_register_batch'):
            batch = Batch.objects.get(pk=int(self.kwargs['pk']))
            if batch.batch_type == PREPARE_FLAGGING:
                queryset = DocumentFile.objects.filter(flagged=True,
                                                       stage=STAGES[0],
                                                       batch_id=int(self.kwargs['pk']))
            else:

                queryset=DocumentFile.objects.filter(flagged=False,
                                        stage=STAGES[0],
                                        batch_id=int(self.kwargs['pk']))
        elif self.request.user.has_perm('app.can_receive_file'):
            batch=Batch.objects.get(pk=int(self.kwargs['pk']))
            if batch.batch_type == PREPARE_FLAGGING:
                queryset=DocumentFile.objects.filter(flagged=True,
                                        stage=STAGES[0],
                                        batch_id=int(self.kwargs['pk']))
            else:
                queryset = DocumentFile.objects.filter(flagged=False,
                                                       stage=STAGES[1],
                                                       batch_id=int(self.kwargs['pk']))


        self.table = BatchFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                  queryset)
        self.table = BatchFileTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['batch_id'] = int(self.kwargs['pk'])
        context['batch_type'] = Batch.objects.get(pk=int(self.kwargs['pk'])).batch_type
        return context


class BatchDocumentsView(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.add_documentfiledetail'
    table_class = BatchDocumentTable
    template_name = 'file_documents_list.html'
    filterset_class = DocumentFilter

    def get_queryset(self):
        queryset = DocumentFileDetail.objects.filter(
            file_reference_id=self.kwargs['file_reference'])
        self.table = BatchDocumentTable(queryset)
        self.filter = DocumentFilter(self.request.GET,queryset)
        self.table = BatchDocumentTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter

        file = get_file(self.request, self.kwargs['file_reference'])


        context['file_ref_no'] = self.kwargs['file_reference']
        return context


def get_file(request, file_ref=None):
    if not file_ref == None:
        file = DocumentFile.objects.get(pk=file_ref)

        if file:

            return file

        elif request.user.has_perm("app.can_register_batch"):
            return file
    return None
def search_batch(request):
    if request.method == 'POST':
       batch_no=request.POST['batch_no']
       try:
           batch=Batch.objects.get(batch_no=batch_no)


           return redirect(reverse('batch_search_retrieve',kwargs={'pk':batch.pk}))

       except Exception as e:
           messages.error(request, ' batch not found')
           return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        data={
            'title1':'Batch',
            'title2':'Reception',
            'post_url': 'batch_search',
            'placeholder':'Batch number',
            'name_attribute':'batch_no'
        }
        return render(request,'batch/search.html',data)

class BatchSearchedListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_batch'
    table_class = BatchTable
    template_name = 'batch/index.html'
    filterset_class = BatchFilter

    def get_queryset(self):
        queryset = Batch.objects.filter(is_return_batch=False,pk=self.kwargs['pk'])

        self.table = BatchTable(queryset)
        self.filter = BatchFilter(self.request.GET,
                                  queryset)
        self.table = BatchTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)
        # return Batch.objects.filter(is_return_batch=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context

def accept_batch(request,pk):
    try:

        batch=Batch.objects.get(pk=pk)
        if batch.batch_type == PREPARE_DIGITIZATION_INTERMEDIARY:
            batch.receive_ready_for_digitization(request.user,approved=True)
            messages.success(request, 'Batch has been made ready for digitization')
        elif batch.batch_type == PREPARE_FLAGGING_INTERMEDIARY:
            batch.receive_prepare_flagging(request.user,approved=True)
            messages.success(request, 'Batch has been moved,ready for fixing errors')

    except Exception:
        messages.error(request, 'Unable to perform this action')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
def reject_batch(request,pk):
    try:

        batch=Batch.objects.get(pk=pk)
        if batch.batch_type == PREPARE_DIGITIZATION_INTERMEDIARY:
            batch.receive_ready_for_digitization(request.user,approved=False)
            messages.success(request, 'Batch has been returned to registry')
        elif batch.batch_type == PREPARE_FLAGGING_INTERMEDIARY:
            batch.receive_prepare_flagging(request.user, approved=False)
            messages.success(request, 'Batch has been returned to reception')
    except Exception :
        messages.error(request, 'Unable to perform this action')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
def add_file_to_batch(request, pk):
    template_name = 'app/batch/add_file_to_batch.html'
    heading_message = 'Add File(s) to Batch'
    if request.method == 'GET':
        formset = AddFileToBatchFormSet(request.GET or None)
    elif request.method == 'POST':
        try:
            formset = AddFileToBatchFormSet(request.POST)

            if formset.is_valid():
                for form in formset:
                        batch = Batch.objects.get(pk=pk)

                        file_barcode = form.cleaned_data.get('file_barcode')
                        file = DocumentFile.objects.get(file_barcode=file_barcode)


                        if batch.batch_type == PREPARE_DIGITIZATION or batch.batch_type == PREPARE_FLAGGING:
                            if file.stage == STAGES[0] and batch.batch_type == PREPARE_DIGITIZATION:
                                file.batch=batch
                                file.save()
                            elif file.stage == STAGES[0] and file.flagged and  batch.batch_type == PREPARE_FLAGGING:
                                file.batch = batch
                                file.save()
                            else:

                                raise PermissionError

                        else:

                            raise PermissionError

                        #sql_logger("Create Document")
                messages.success(request, 'File(s) added successfully')
                return redirect(reverse('batch_files', kwargs={'pk': pk}))
        except Exception as e:

            messages.error(request, 'Access Denied,some files could not be added')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return render(request, template_name, {
        'formset': formset,
        'batch_id':pk,
        'heading': heading_message,
    })



