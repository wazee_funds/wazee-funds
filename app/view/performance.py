from app.models import DocumentFile, Modification
from django.contrib.auth.models import User



# get each user

def get_user_list():
    users = User.objects.all()
    user_ids = []
    for user in users:
        user_ids.append(user.id)
    return user_ids
