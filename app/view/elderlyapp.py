import os
import json
from pathlib import Path
from django.contrib import messages
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.views import PasswordChangeForm
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
import urllib.parse
from django.contrib.auth.views import LoginView
from datetime import date
from django.views.decorators.csrf import csrf_exempt
import subprocess
from app.models import Modification
from django.contrib.auth.models import Group, User, Permission
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django_filters.views import FilterView
from django_tables2 import RequestConfig
from django_tables2.views import SingleTableMixin

from app.mixin import LoggedInRedirectMixin

from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView
from django.views.generic.base import View
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django_jsonforms.forms import JSONSchemaForm
import urllib
from json2html import *

from app.decorators import unauthenticated_user
from app.forms import LoginForm, UserRegistrationForm, \
    PasswordResetForm, GroupCreationForm,ElderyRegistrationForm
from app.models import DocumentFile, DocumentFileType, DocumentType, DocumentFileDetail, Batch, STATES, STAGES,Money_disburdment
from app.tables import DocumentTable, AgentUserTable, GroupTable, CleanUpTable,DispatchTable,AuditorUserTable
from app.filters import DocumentFilter, AgentUserFilter,DispatchFilter
from app.view.logged_in_users import get_current_users
from app.tasks import clean_up_split_files
from django.core.mail import send_mail
from django.conf import settings
from background_task import background
from bank.models import Account,Deposit,Withdraw
def mark_as_dead(request,pk):

    user=User.objects.get(pk=pk)

    dead_group=Group.objects.filter(permissions__codename='is_dead').first()

    try:


        user.groups.clear()
        user.groups.add(dead_group)


        messages.success(request, 'UserInformation Changed Successfully')
    except Exception:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
def guardian_create(request,elderly_id):
    if request.method == 'POST':
        form = ElderyRegistrationForm(request.POST)
        if form.is_valid():
            id_no = form.cleaned_data.get('id_no')
            email = form.cleaned_data.get('email')
            username = id_no
            password = id_no
            group = None
            try:
                group = Group.objects.get(name='Guardian')
            except Exception  as e:
                messages.error(request, e)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

            user = User.objects.create_user(username, email, password)
            user.groups.add(group)
            user.refresh_from_db()
            user.profile.id_no = form.cleaned_data.get('id_no')
            user.profile.phone = form.cleaned_data.get('phone')
            user.profile.full_name = form.cleaned_data.get('full_name')
            user.profile.elderly_id =elderly_id
            user.save()
            messages.success(request, f'User created successfully!')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


    else:
        form = ElderyRegistrationForm()
    return render(request, 'create_elderly.html', {
        'form': form,
    })
def add_business_name(request,user_pk):
    if request.method == 'POST':
        user=User.objects.get(pk=user_pk)
        business_name=request.POST['business_name']
        user.last_name=business_name
        user.save()
        messages.success(request, f'Business added successfully!')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def disburse(request):
    today = date.today()
    mnth=today.month
    yr=today.year
    accounts=Account.objects.all()
    account_owners=[]
    for account in accounts:
        depo=Deposit.objects.filter(account_id=account.pk,at__month=mnth,at__year=yr)
        if not depo:
            add_depo=Deposit.objects.create(account_id=account.pk,month=mnth,year=yr,amount=4000,balance=account.balance)
            add_depo.deposit_now()
            account_owners.append(account.user)

    receivers_emails=[]
    for owner in account_owners:
        receivers_emails.append(owner.email)
        guardians = User.objects.filter(profile__elderly_id=owner.pk)
        for guardian in guardians:
            receivers_emails.append(guardian.email)
    content='Elderly Funds has been dispatched to your account'
    subject='Ederly Funds'

    send_email(receiver=receivers_emails,subject=subject,message=content)
    process_task()
    messages.success(request, 'Funds Dispatched Successfully')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
@background(schedule=1)
def send_email(receiver,subject,message):
    subject = subject
    message = message
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [*receiver]
    send_mail( subject, message, email_from, recipient_list )
    return 'new email sent'

class DispatchListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = DispatchTable
    template_name = 'dispatch.html'
    filterset_class = DispatchFilter

    def get_queryset(self):
        queryset = Money_disburdment.objects.filter(user=self.request.user)


        if self.request.user.is_superuser:
            queryset=Money_disburdment.objects.all()
        self.table = DispatchTable(queryset)
        self.filter = DispatchFilter(self.request.GET,
                                 queryset)
        self.table = DispatchTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter

        return context

class AgentUserListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = AgentUserTable
    template_name = 'users.html'
    filterset_class = AgentUserFilter

    def get_queryset(self):
        queryset = User.objects.all()

        user_type=self.kwargs['user_type']
        if user_type:
            int(user_type)

            if user_type == 21:

                queryset=User.objects.filter(groups__permissions__codename__in=['is_elderly'],is_superuser=False,
                                             account__isnull=False,first_name__isnull=False,first_name=self.request.user.first_name)

                print(queryset)
        self.table = AgentUserTable(queryset)
        self.filter = AgentUserFilter(self.request.GET,
                                 queryset)
        self.table = AgentUserTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['user_type']=self.kwargs['user_type']
        return context

class AuditorUserListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = AuditorUserTable
    template_name = 'users.html'
    filterset_class = AgentUserFilter
    total_disburcement=0

    def get_queryset(self):
        queryset = User.objects.all()

        user_type=self.kwargs['user_type']
        if user_type:
            int(user_type)
            if user_type == 22:

                queryset=User.objects.filter(groups__permissions__codename__in=['is_elderly'],is_superuser=False,account__isnull=False)
        for q in queryset:
            if not isinstance(q.account.deposit_amount,str):
                self.total_disburcement+=q.account.deposit_amount


        self.table = AuditorUserTable(queryset)
        self.filter = AgentUserFilter(self.request.GET,
                                 queryset)
        self.table = AuditorUserTable(self.filter.qs)

        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['user_type']=self.kwargs['user_type']
        context['total_disburcement']=self.total_disburcement
        return context





def process_task():
    process = subprocess.Popen(['python', 'manage.py', 'process_tasks'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(process)
    return


def transactions(request,user_pk):
    acc_owner=User.objects.get(pk=user_pk)
    acc=acc_owner.account
    deposits=Deposit.objects.filter(account_id=acc.pk)
    withdrawals=Withdraw.objects.filter(account_id=acc.id)

    data={'deposits':deposits,
          'withdrawals':withdrawals}

    return render(request,'transactions/deposits.html',data)



