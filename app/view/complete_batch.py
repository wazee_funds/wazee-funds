from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, RequestConfig

from app.models import CompleteBatch, DocumentFile, STAGES
from app.tables import CompleteBatchTable, BatchFileTable, CompleteBatchFileTable
from app.filters import CompleteBatchFilter, DocumentFileFilter


class CompleteBatchListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    permission_required = 'app.view_batch'
    table_class = CompleteBatchTable
    template_name = 'batch/complete_batch.html'
    filterset_class = CompleteBatchFilter

    def get_queryset(self):
        queryset = CompleteBatch.objects.all()
        self.table = CompleteBatchTable(queryset)
        self.filter = CompleteBatchFilter(self.request.GET,
                                          CompleteBatch.objects.all())
        self.table = CompleteBatchTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)
        # return Batch.objects.filter(is_return_batch=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        return context


# get files of a completed batch
class CompleteBatchFilesView(LoginRequiredMixin, SingleTableMixin, FilterView):
    template_name = 'batch/filetable.html'
    table_class = CompleteBatchFileTable
    filterset_class = DocumentFileFilter

    def get_queryset(self):
        queryset = DocumentFile.objects.none()
        if self.request.user.is_superuser:
            queryset=DocumentFile.objects.filter(flagged=False, batch_id=int(self.kwargs['pk']))
        elif self.request.user.has_perm('app.can_register_batch'):

           queryset= DocumentFile.objects.filter(stage=STAGES[0],
                                                 batch_id=int(self.kwargs['pk']),
                                                 flagged=False)
        elif self.request.user.has_perm('app.can_receive_file'):

            q1 = DocumentFile.objects.filter(stage=STAGES[1],
                                             batch_id=int(self.kwargs['pk']),
                                             flagged=False,
                                             assigned_to=self.request.user)
            q2 = DocumentFile.objects.filter(stage=STAGES[1],
                                             batch_id=int(self.kwargs['pk']),
                                             flagged=False,
                                             assigned_to=None)
            queryset= q1.union(q2)
        self.table = CompleteBatchFileTable(queryset)
        self.filter = DocumentFileFilter(self.request.GET,
                                  queryset)

        self.table = CompleteBatchFileTable(self.filter.qs)
        RequestConfig(self.request, paginate={'per_page': 10}).configure(self.table)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['table'] = self.table
        context['filter'] = self.filter
        context['batch_id'] = int(self.kwargs['pk'])
        return context