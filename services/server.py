import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from PyPDF2 import PdfFileReader, PdfFileWriter

split_pdf_directory = "pages"
base_pdf_directory = ""
pdfa_directory = ""

def read_pdf():
    number_of_pages = PdfFileReader.getNumPages()


def split_pdf():
    pdf = PdfFileReader(path)
    for page in range(pdf.getNumPages()):
        pdf_writer = PdfFileWriter()
        pdf_writer.addPage(pdf.getPage(page))

        output = f'{name_of_split}{page}.pdf'
        with open(output, 'wb') as output_pdf:
            pdf_writer.write(output_pdf)


def append_pdf():
    pdf_writer = PdfFileWriter()

    for path in paths:
        pdf_reader = PdfFileReader(path)
        for page in range(pdf_reader.getNumPages()):
            # Add each page to the writer object
            pdf_writer.addPage(pdf_reader.getPage(page))

    # Write out the merged PDF
    with open(output, 'wb') as out:
        pdf_writer.write(out)


class OnMyWatch:
    # Set the directory on watch
    watchDirectory = storage_directory

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.watchDirectory, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Observer Stopped")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Event is created, you can process it now
            print("Watchdog received created event - % s." % event.src_path)

            upload_files(event.src_path)
            # archive(event.src_path)
            # move file to archived folder
        elif event.event_type == 'modified':

            # Event is modified, you can process it now
            print("Watchdog received modified event - % s." % event.src_path)
            # upload_files(event.src_path)


if __name__ == '__main__':
    watch = OnMyWatch()
    watch.run()