import os
import time
import shutil
import requests
from pathlib import Path
from win10toast import ToastNotifier
from datetime import datetime
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import subprocess

base_url = "http://41.206.54.242:31095"
filebarcode_url = "/api-v1/rest/"
upload_url = "/api-v1/documentfile_upload_zipped"
storage_directory = os.path.expanduser('~/Documents/EDMS')  # root storage directory
archive_directory = os.path.expanduser('~/Documents/EDMSA/')
Path(storage_directory).mkdir(parents=True, exist_ok=True)
Path(archive_directory).mkdir(parents=True, exist_ok=True)
toaster = ToastNotifier()


def upload_files(file):
    file = Path(file)
    base = os.path.basename(file)
    barcode = os.path.splitext(base)[0]
    print(base)
    payload = {'file_barcode': barcode}
    try:
        open_file = open(file, 'rb')
        files = {'filepond': open_file}

        r = requests.post(base_url + upload_url,
                          data=payload,
                          files=files
                          )

        if r.status_code == 201:

            toaster.show_toast("EDMS Butler",
                           "File has been uploaded",
                           icon_path='lions.ico',
                           duration=5,
                           threaded=True)
            archive(file)
        else:
            toaster.show_toast("EDMS Butler",
                               "Please try to manually upload the file",
                               icon_path='lions.ico',
                               duration=5,
                               threaded=True)

        open_file.close()


    except  Exception as exc:
        print(exc)
        toaster.show_toast("EDMS Butler Exception",
                           "Error" + exc,
                           icon_path='lions.ico',
                           duration=5,
                           threaded=True)
    archive(file)


def archive(file):
    try:
        basename = os.path.basename(file)
        barcode = os.path.splitext(basename)[0]
        extension = os.path.splitext(basename)[1]
        newname = barcode + datetime.today().strftime('%Y-%m-%d') + extension
        destination = archive_directory + newname
        destiny = shutil.move(file, Path(destination))
        toaster.show_toast("EDMS Butler",
                           "File Archived",
                           icon_path='lions.ico',
                           duration=5,
                           threaded=True)
    except Exception as exc:
        toaster.show_toast("EDMS Butler Exception",
                           "Error" + exc,
                           icon_path='lions.ico',
                           duration=5,
                           threaded=True)


class OnMyWatch:
    # Set the directory on watch
    watchDirectory = storage_directory

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.watchDirectory, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Observer Stopped")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Event is created, you can process it now
            print("Watchdog received created event - % s." % event.src_path)

            upload_files(event.src_path)
            # archive(event.src_path)
            # move file to archived folder
        elif event.event_type == 'modified':

            # Event is modified, you can process it now
            print("Watchdog received modified event - % s." % event.src_path)
            # upload_files(event.src_path)


def edms_butler():
    watch = OnMyWatch()
    watch.run()

if __name__ == '__main__':
    watch = OnMyWatch()
    watch.run()


