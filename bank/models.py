
from django.utils import timezone

from django.contrib.auth.models import User

from django.db import models

class Account(models.Model):
    class Meta:
        permissions = [('can_handle_bank', 'Can Do Bank Details '),('can_audit', 'Can Do Auditing')]

    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                db_index=True, db_tablespace='pg_default')
    balance=models.DecimalField(max_digits=12,decimal_places=2,default=0)

    def __str__(self):
        return self.user.username + '   -> Ksh '+str(self.balance)
    @property
    def deposit(self):
        return Deposit.objects.filter(account=self).last()

    @property
    def deposit_month(self):
        if self.deposit:
            return self.deposit.month
        else:
            return '----'

    @property
    def deposit_year(self):
        if self.deposit:
            return self.deposit.year
        else:
            return '----'

    @property
    def deposit_amount(self):
        if self.deposit:
            return self.deposit.amount
        else:
            return '----'

    @property
    def deposit_at(self):
        if self.deposit:
            return self.deposit.at
        else:
            return '----'
    def update_balance(self,amount):
      self.balance=self.balance + amount
      self.save()
      return self.balance



class Deposit(models.Model):
    account=models.ForeignKey(Account,on_delete=models.CASCADE,
                                db_index=True, db_tablespace='pg_default')

    month=models.CharField(max_length=255)
    year = models.CharField(max_length=255)
    amount=models.DecimalField(max_digits=12,decimal_places=2)
    balance = models.DecimalField(max_digits=12, decimal_places=2)
    at = models.DateTimeField(auto_now_add=timezone.now)

    def deposit_now(self):
        self.balance=self.account.update_balance(self.amount)
        self.save()


class Withdraw(models.Model):
    account=models.ForeignKey(Account,on_delete=models.CASCADE,
                                db_index=True, db_tablespace='pg_default')
    by_user = models.ForeignKey(User, on_delete=models.DO_NOTHING,
                                   related_name='by_user', db_index=True, db_tablespace='pg_default',related_query_name='by_user')

    by_agent = models.ForeignKey(User, on_delete=models.DO_NOTHING,
                                   related_name='by_agent', db_index=True, db_tablespace='pg_default',related_query_name='by_agent')

    amount=models.DecimalField(max_digits=12,decimal_places=2)
    balance = models.DecimalField(max_digits=12, decimal_places=2)
    at = models.DateTimeField(auto_now_add=timezone.now)

    def withdraw_now(self):
        self.balance=self.account.update_balance(-self.amount)
        self.save()




