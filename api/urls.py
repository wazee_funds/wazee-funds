
from django.urls import path
from django.conf.urls import url
from .views import (
    DocumentFileDetailView,
    FileUploadView,
    search_barcode
)

urlpatterns = [

    path('documentfile/<file_barcode>', DocumentFileDetailView.as_view()),
    path('documentfile_upload_zipped', FileUploadView.as_view()),
    path('rest/<str:barcode>', search_barcode, name="barcode_search")

]

